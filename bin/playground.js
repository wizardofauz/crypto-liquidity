#!/usr/bin/env node
require('../server.babel'); // babel registration (runtime transpilation for node)
const api = require('../playground/contract');

module.exports = api;
