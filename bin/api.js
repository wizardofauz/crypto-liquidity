#!/usr/bin/env node
require('../server.babel'); // babel registration (runtime transpilation for node)
const api = require('../src/server/api/main');

module.exports = api;
