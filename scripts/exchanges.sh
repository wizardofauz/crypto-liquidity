#!/bin/bash
declare -a coins=("BINANCE" "HUOBI" "OKEX" "BITFINEX");

for i in "${coins[@]}"
do
     declare string
     string=`echo "$i" | tr '[:upper:]' '[:lower:]'`
     echo "$string"
     wget "https://cryptocoincharts.info/img/exchanges/${string}.svg"
done

