#!/bin/bash
declare -a coins=("BTC" "ETH" "XRP" "BCH" "EOS" "LTC" "IOTA" "NEO" "ETC" "MGO" "ZEC" "XMR" "DASH" "ZRX" "ETP" "OMG" "BTG" "TRX" "EDO" "DAI" "XLM" "XTZ" "BAT" "CNN" "DAD" "QTUM" "SAN" "MKR" "LYM" "ELF" "BBN" "BFT" "DAT" "YOYOW" "UTK" "IOT" "XVG" "GNT" "WAX" "SEE" "SEN" "ODE" "MAN" "QSH" "YGG" "REP" "DTH" "MANA" "KNC" "TKN" "AUC" "VET" "RTE" "SNT" "REQ" "PKGO" "ESS" "WPR" "AGI" "IQX" "ZIL" "FUN" "RLC" "RCN" "UTN" "RRT" "INT" "AIO" "IOS" "TNB" "SNG" "RDN" "SPK" "BCI" "MLN" "AVT" "PAI" "HOT" "MIT" "ANIO" "XRA" "MTN" "POY" "DGX" "MLM" "ORS" "AID" "STJ" "FSN" "VEE" "ABS" "POA" "LRC" "ATM" "DGB" "CTX" "DTA" "WTC" "CND" "ZCN" "PNK" "BNT" "ANT" "BOX" "CBT" "DRK" "BT2" "BFX" "BTU" "BT1" "BCC" "TH1" "CF" "CFI" "VEN" "STK" "HT" "ONT" "USDT" "WICC" "PROPY" "IOST" "ADA" "BTM" "EDU" "SWFTC" "MEET" "XZC" "POWR" "OCN" "CTXC" "ELA" "GXS" "POLY" "RUFF" "DCR" "GNX" "HB10" "NAS" "THETA" "LBA" "BLZ" "BIX" "ITC" "KAN" "SOC" "HIT" "STORJ" "SRN" "CMT" "CVC" "SBTC" "WAN" "TOPC" "LET" "XEM" "PAY" "SNC" "TNT" "DBC" "BTS" "ACT" "LINK" "ZLA" "APPC" "MCO" "UUU" "OST" "SMT" "WAVES" "CHAT" "AST" "LSK" "TRIO" "LUN" "STEEM" "SSP" "NCASH" "GSC" "HPT" "SHE" "MTX" "AE" "METAL" "ADX" "QASH" "EVX" "QSP" "SALT" "ABT" "MDS" "GAS" "YEE" "SEELE" "UIP" "QUN" "AIDOC" "PHX" "DGD" "PNT" "PORTAL" "ENG" "UC" "ICX" "BCX" "EKT" "FTI" "NANO" "IIC" "MUSK" "YCC" "GTC" "GET" "GVE" "MEX" "BKBT" "CVCOIN" "XMX" "BCD" "PC" "HC" "EGCC" "18C" "MT" "DAC" "IDT" "BUT" "FAIR" "DATX" "ZJLT" "LXT" "REN" "ARDR" "GRS" "RCCC" "BCV" "NCC" "AAC" "BIFI" "KCASH" "TOS" "CDC" "EKO" "HSR" "MEE" "RPX" "OKB" "GTO" "TRUE" "PAX" "HYC" "MITH" "PST" "OF" "DPY" "ORST" "HPB" "TUSD" "DADI" "BKX" "RCT" "ZIP" "EGT" "LIGHT" "AUTO" "GUSD" "CAI" "MOF" "ZCO" "TCT" "VITE" "ABL" "INS" "MDT" "VIB" "BEC" "MVP" "UGC" "UCT" "RFR" "XUC" "ACE" "STC" "NULS" "MDA" "SHOW" "ZEN" "CAN" "RNT" "INSUR" "CVT" "SPF" "ENJ" "WIN" "SC" "R" "POE" "PRA" "YOU" "SSC" "KEY" "MAG" "USDC" "IPC" "ARK" "BCN" "PPT" "LEV" "CIC" "CIT" "XAS" "1ST" "VIU" "DENT" "LEND" "NGC" "SDA" "UBTC" "TRA" "WRC" "AMM" "OAX" "MTH" "DNT" "SUB" "MOT" "OK06ETT" "SNGLS" "DNA" "NXT" "REF" "ICN" "SNM" "HMC" "CAG" "BRD" "UKG" "ATL" "LA" "WFEE" "TIO" "READ" "CTR" "WBTC" "QVT" "RVN" "BNB" "ARN" "QKC" "GO" "DOCK" "MFT" "NPXS" "GVT" "IOTX" "MOD" "STORM" "BCPT" "AION" "CDT" "DLT" "ETHOS" "FUEL" "WABI" "LOOM" "NEBL" "STRAT" "KMD" "VIBE" "DATA" "AMB" "PIVX" "QLC" "NXS" "VIA" "NAV" "WINGS" "SKY" "CLOAK" "SYS" "TRIG" "LLT" "ELC" "HCC");

for i in "${coins[@]}"
do
     declare string
     string=`echo "$i" | tr '[:upper:]' '[:lower:]'`
     wget -q "https://cryptocoincharts.info/img/coins/${string}.svg" && echo "import $string from './$string.svg'" >> index.js
done

echo "export default { " >> index.js
for i in ls -A1
do
     declare string
     string=`echo "${$i/\.svg/}"`
     echo "$string," >> index.js
done

echo "};" >> index.js
