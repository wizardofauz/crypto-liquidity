_Requirements:_

- Node.js v8+
- npm v5+

_DEV Setup_

```npm install```

in 2 separate terminal processes, run the command:
- ```npm run dev:ui```  
- ```npm run dev:server```

_PROD Setup_

```npm install```
```npm run build```
```docker build .```

Test the output using the command
```npm run docker```
