const fs = require('fs');

const babelrc = fs.readFileSync('./.babelrc');
let config;

try {
  config = JSON.parse(babelrc);
} catch (err) {
  console.error('==>     ERROR: Error parsing your .babelrc.');
  console.error(err);
}

require('babel-register')(config);
require('babel-polyfill');
const webpackConfig = require('../webpack/ui/dev.config.babel');
module.exports = (storybookBaseConfig, configType) => {

  webpackConfig.module.rules.forEach(rule => {
    storybookBaseConfig.module.rules.push(rule);
  })

  return storybookBaseConfig;
};
