const fs = require('fs');

const babelrc = fs.readFileSync('./.babelrc');
let config;

try {
  config = JSON.parse(babelrc);
  config.env.development.plugins.push(
    ['module-resolver', {
      root: ['./src/server'],
    }],
  );
} catch (err) {
  console.error('==>     ERROR: Error parsing your .babelrc.');
  console.error(err);
}

require('babel-register')(config);
require('babel-polyfill');
