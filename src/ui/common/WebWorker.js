import Worker from 'worker-loader!../worker.js';

export default class WebWorker {
  constructor() {
    if (!window.workerInstance) {
      window.workerInstance = new Worker();
    }
    this.instance = window.workerInstance;
  }

  getInstance() {
    return this.instance;
  }

  send(data) {
    if (this.instance) {
      this.instance.postMessage({ type: 'sendData', data });
    }
  }
}
