export const UNABLE_TO_RECONNECT = 'Unable to reconnect';
export const CONNECTING = 'Connecting';
export const CONNECTED = 'Connected';
export const DISCONNECTED = 'Disconnected';
export const RECONNECTING = 'Reconnecting';

export const STATUS_SEVERITY_MAP = {
  'Unable to reconnect': 'error',
  'Connecting': 'info',
  'Connected': 'success',
  'Disconnected': 'warning',
  'Reconnecting': 'info',
};

const EXCHANGE_NAME_OVERRIDES = {
  'huobipro': 'huobi'
};

export const EXCHANGE_NAME_MAP_FOR_LOGO = (exchange) => {
  if (EXCHANGE_NAME_OVERRIDES[exchange]) {
    return EXCHANGE_NAME_OVERRIDES[exchange];
  }
  else return exchange;
};
