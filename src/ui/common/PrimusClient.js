/* eslint-disable no-console */
import { isObject } from 'lodash';
import { CONNECTED, DISCONNECTED, CONNECTING, UNABLE_TO_RECONNECT } from './constant';

const primusFileName = 'primus.js';
const isDev = process.env.NODE_ENV === 'development';

export default class PrimusClient {
  constructor(origin, context, middlewares = {}) {
    this.init = this.init.bind(this);
    this.applyMiddlewares = this.applyMiddlewares.bind(this);
    this.middlewares = middlewares;

    this.context = context;
    context.importScripts(`${origin}${primusFileName}`);

    const url = origin;

    return Promise.resolve(new context.Primus(url, {
      reconnect: {
        retries: 5,
      },
    }))
      .then(instance => {
        this.instance = instance;
      })
      .then(this.init)
      .catch(e => {
        throw e;
      });
  }

  write(data) {
    if (this.instance) {
      this.instance.write(data);
    }
  }

  applyMiddlewares(msg) {
    const { type } = msg;
    if (type && this.middlewares[type] && typeof this.middlewares[type] === 'function') {
      if (isDev) {
        console.debug('[Primus]: applying middleware:', type); // eslint-disable-line no-console
      }
      return this.middlewares[type](msg);
    }
    return Promise.resolve(msg);
  }

  init() {
    Object.keys(this.instance.reserved.events).filter(event => event !== 'data')
      .forEach(event => {
        this.instance.on(event, msg => {
          if (msg !== undefined) console.debug('[Primus]:', msg);
          if (msg === 'end') {
            this.context.postMessage({
              type: 'data',
              data: { type: 'primus-status', data: DISCONNECTED }
            });
          }
          if (msg === 'opening') {
            this.context.postMessage({
              type: 'data',
              data: { type: 'primus-status', data: CONNECTING }
            });
          }
          this.instance.on('open', () => {
            this.context.postMessage({
              type: 'data',
              data: { type: 'primus-status', data: CONNECTED }
            });
          });
          this.instance.on('reconnect failed', () => {
            console.debug('primus-error', 'reconnect failed');
            this.context.postMessage({
              type: 'data',
              data: { type: 'primus-status', data: UNABLE_TO_RECONNECT }
            });
          });
        });
      });

    this.instance.on('error', msg => {
      if (msg instanceof Error || (isObject(msg) && msg.attempt === msg.retries)) {
        console.debug('Primus error', msg, this.instance.readyState);
      }
    });

    this.instance.on('data', message => {
      if (isDev) {
        console.debug('[Primus]: data:', message.type || message); // eslint-disable-line no-console
      }
      this.applyMiddlewares(message).then(msg => {
        if (msg.type === 'finish') {
          this.context.postMessage('finish');
        } else if (msg.type) {
          this.context.postMessage({ type: 'data', data: msg });
        }
      });
    });
    return this;
  }

  destroy() {
    this.instance.destroy();
  }
}
