export const waitFor = async(reference, path, checkInterval) => {
  if (! reference[path]) {
    await new Promise(resolve => setTimeout(resolve, checkInterval));
    await waitFor(reference, path, checkInterval);
  }
  return reference[path];
};

export const arrayEqual = (array1, array2) => {
  if (!array2)
    return false;

  // compare lengths - can save a lot of time
  if (array1.length !== array2.length)
    return false;

  for (let i = 0, l = array1.length; i < l; i++) {
    // Check if we have nested array2s
    if (array1[i] instanceof Array && array2[i] instanceof Array) {
      // recurse into the nested array2s
      if (!array1[i].equals(array2[i]))
        return false;
    }
    else if (array1[i] !== array2[i]) {
      // Warning - two different object instances will never be equal: {x:20} != {x:20}
      return false;
    }
  }
  return true;
};

export const chunkArrayInGroups = (arr, size) => {
  const results = [];
  while (arr.length) {
    results.push(arr.splice(0, size));
  }
  return results;
};

export const throttled = (fn, delay) => {
  let lastCall = 0;
  return function (...args) {
    const now = (new Date).getTime();
    if (now - lastCall < delay) {
      return;
    }
    lastCall = now;
    return fn(...args);
  }
};
