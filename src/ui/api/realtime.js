import WebWorker from '../common/WebWorker';
const worker = new WebWorker();

export const getRTStatus = async () => {
  worker.send({
    type: 'status',
  });
};

export const getTickers = async () => {
  worker.send({
    type: 'pairs',
  });
};

export const getPair = async (tickerLeft, tickerRight) => {
  worker.send({
    type: 'tickerPair',
    data: { tickerLeft, tickerRight },
  });
};

export const createInterest = async (interestId, tickerPair, volume, direction, exchanges) => {
  worker.send({
    type: 'interest',
    data: { interestId, tickerPair, volume, direction, exchanges },
  });
};

export const axeInterest = async (interestId) => {
  worker.send({
    type: 'axe_interest',
    data: { interestId },
  })
};

export const modifyInterest = async (interestId, volume, direction) => {
  worker.send({
    type: 'modify_interest',
    data: { interestId, volume, direction },
  })
};

export const sendFeedback = async (payload) => {
  worker.send({
    type: 'feedback',
    data: { payload }
  })
};
