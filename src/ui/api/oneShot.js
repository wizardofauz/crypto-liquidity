const handleErrors = (response) => {
  if (!response.ok) {
    if (response.status === 401) { // Status code: Unauthorized
      // eslint-disable-next-line no-console
      console.error(response);
    }
    throw response;
  }
  return response;
};

const getBasePath = ({ pathname }) => (pathname.substring(0, pathname.lastIndexOf('/') + 1));
const getOrigin = ({ location: { origin, pathname } }) => (`${origin}${getBasePath({ pathname })}`);

export const fetchStatus = () => {
  return fetch(`${getOrigin(window)}/status`)
    .then(handleErrors)
    .then(response => {
      return response.json();
    });
};
