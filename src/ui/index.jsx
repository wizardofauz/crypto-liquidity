import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter, Route } from 'react-router-dom';
import * as Sentry from '@sentry/browser';
import ReactGA from 'react-ga';
import withTracker from './store/analytics';
import { welcomeText } from './common/welcome';
import store from './store/index';
import App from './containers/App';
import WebWorker from './common/WebWorker';
import mainHandler from './rt/mainHandler';

ReactGA.initialize('G-1EX0RBSNTK', {
  titleCase: false,
  gaOptions: {}
});
if (process.env.NODE_ENV != 'dev') {
  Sentry.init({
    dsn: "https://07117c8703f9423e9041f8c91a675321@sentry.io/1314045",
    release: __COMMIT_HASH__
  });
}

const instantiateMainWorker = () => {
  const worker = (new WebWorker()).getInstance();
  worker.addEventListener('message', mainHandler(store, worker));
  worker.postMessage({ type: 'open' });
};
if (typeof (window) !== 'undefined' && window.Worker) {
  instantiateMainWorker();
}

ReactDOM.render(
  <Provider store={store}>
    <HashRouter>
      <Route path="/" component={withTracker(App)} />
    </HashRouter>
  </Provider>,
  document.getElementById('main'),
);

console.log(`%c ${welcomeText}`, 'color: #bada55; font-size: 32px');
