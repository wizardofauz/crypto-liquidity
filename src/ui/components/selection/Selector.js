import React from 'react';
import { connect } from 'react-redux';
import { Button, Spin, Row, Col } from 'antd';
import TickerInput from '../common/TickerInput';
import { referenceDataActions } from '../../reducers/referenceData';

class Selector extends React.PureComponent {
  constructor(props) {
    super(props);
    this.firstInput = React.createRef();
    this.state = {
      tickerLeft: undefined,
      tickerRight: undefined,
      dirty: false,
      swapped: false,
    };
  }

  componentDidMount() {
    if (this.firstInput && this.firstInput.current) {
      this.firstInput.current.focus();
    }
  }

  componentDidUpdate() {
    const { tickerLeft, tickerRight, dirty } = this.state;
    const { fetchPair } = this.props;
    if (dirty && tickerLeft && tickerRight)  {
      fetchPair(tickerLeft, tickerRight);
      this.setState({ dirty: false });
    }
  }

  onSwap = () => {
    const { tickerLeft, tickerRight, swapped } = this.state;
    this.setState({
      tickerRight: tickerLeft,
      tickerLeft: tickerRight,
      dirty: true,
      swapped: !swapped
    });
  };

  render() {
    const { referenceData } = this.props;
    const { tickers, pairs } = referenceData;
    const { swapped, tickerLeft } = this.state;

    let refinedRightTickerSelection = undefined;
    if (tickerLeft) {
      const allowedPairs = pairs[tickerLeft.symbol];
      refinedRightTickerSelection = tickers.filter(ticker => allowedPairs.includes(ticker.symbol));
    }

    const tickerComponents = [
      (<TickerInput
        key="ticker-input-left"
        style={{ margin: '2rem 0' }}
        tickers={tickers}
        ref={this.firstInput}
        tabIndex={ swapped ? 2 : 1 }
        onTickerSelected={_ => {
          const { swapped } = this.state;
          const side = swapped
            ? { tickerRight: _, dirty: true }
            : { tickerLeft: _, dirty: true };
          this.setState(side)
        }}/>),
      (<TickerInput
        key={`ticker-input-right${refinedRightTickerSelection ? `-refined-${refinedRightTickerSelection.join('-')}`: ''}`}
        style={{ margin: '2rem 0' }}
        tickers={refinedRightTickerSelection || tickers}
        tabIndex={ swapped ? 1 : 2 }
        onTickerSelected={_ =>
        {
          const { swapped } = this.state;
          const side = swapped
            ? { tickerLeft: _, dirty: true }
            : { tickerRight: _, dirty: true };
          this.setState(side)
        }}/>)
    ];
    return (
      <div
        style={{ padding: '1rem', textAlign: 'center', marginLeft: 'auto', marginRight: 'auto' }}>
        {
          tickerComponents[swapped ? 1 : 0]
        }
        <Button
          type="dashed"
          tabIndex="-1"
          shape="circle"
          icon="swap"
          size="small"
          onClick={() => this.onSwap() }
          htmlType="button" />
        {
          tickerComponents[swapped ? 0 : 1]
        }
      </div>
    );
  }

}

const mapStateToProps = state => ({
  referenceData: state.referenceData
});
const mapDispatchToProps = dispatch => ({
  fetchPair: (tickerLeft, tickerRight) => dispatch(referenceDataActions.fetchPair(tickerLeft, tickerRight)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Selector);
