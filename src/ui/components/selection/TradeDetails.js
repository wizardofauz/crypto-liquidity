import React from 'react';
import { connect } from 'react-redux';
import { Button, Icon, Select } from 'antd';
import uuid from 'uuid/v4';
import { arrayEqual } from '../../common/util';
import { interestActions } from '../../reducers/interest';
import { EXCHANGE_NAME_MAP_FOR_LOGO } from '../../common/constant';
import exchangesImages from '../../images/exchanges';
import GoogleAnalytics from 'react-ga';

const Option = Select.Option;

class TradeDetails extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      id: uuid(),
      direction: 'BUY',
      volume: 10000,
      availableExchanges: undefined,
    };
  }

  componentDidMount() {
    const { referenceData } = this.props;
    const { availableExchanges } = referenceData;
    this.setState({
      availableExchanges,
      checkedExchangesList: availableExchanges
    });
  }

  componentDidUpdate(prevProps) {
    const { referenceData } = this.props;
    const { availableExchanges, tickerPair } = referenceData;
    const { tickerHash } = this.state;

    let cumulativeStateChange = {};
    if (! arrayEqual(availableExchanges, prevProps.availableExchanges)) {
      cumulativeStateChange = {
        ...cumulativeStateChange,
        ...{ availableExchanges },
      };
    }
    if (tickerPair && tickerPair.left && tickerPair.right) {
      const newHash = `${tickerPair.left.symbol}-${tickerPair.right.symbol}`;
      if (newHash !== tickerHash) {
        cumulativeStateChange = {
          ...cumulativeStateChange,
          ...{
            checkedExchangesList: availableExchanges,
            tickerHash: newHash,
          }
        };
      }
    }
    if (Object.keys(cumulativeStateChange).length > 0) {
      this.setState(cumulativeStateChange);
    }
  }

  onExchangesChange = (checkedList) => {
    this.setState({
      checkedExchangesList: checkedList,
    });
  };

  onCreateInterest = () => {
    const { referenceData, createInterest } = this.props;
    const { direction, id, checkedExchangesList, volume, availableExchanges } = this.state;
    const { tickerPair } = referenceData;
    createInterest(id, tickerPair, volume, direction, checkedExchangesList || availableExchanges);

    GoogleAnalytics.event({
      category: 'Interest',
      action: 'Created interest',
      value: `${tickerPair.left.symbol}-${tickerPair.right.symbol}`
    });
    // Resetting the UUID so a new interest can be created
    this.setState({
      id: uuid(),
    });
  };

  render() {
    const { referenceData } = this.props;
    const { availableExchanges, tickerPair } = referenceData;
    const { tickerHash } = this.state;

    if (tickerPair && tickerPair.invalid) {
      return (
        <div style={{ textAlign: 'center', color: 'tomato' }}>
          <Icon type="warning" style={{ fontSize: '24px', margin: '0 1rem'}} theme="twoTone" twoToneColor="#ff4206"/>
          <h4>Invalid symbol, please adjust</h4>
        </div>
      );
    }

    if (! availableExchanges || availableExchanges.length < 1) {
      return (
        <div style={{ textAlign: 'center', color: 'tomato' }}>
          <Icon type="warning" style={{ fontSize: '24px', margin: '0 1rem'}} theme="twoTone" twoToneColor="#ff4206"/>
          <h4>{`Invalid symbol, no exchange support for
          ${tickerPair.left.symbol}/${tickerPair.right.symbol}`}</h4>
        </div>
      );
    }

    return (
      <div
        key={`details-${tickerHash}`}
        style={{ textAlign: 'center', verticalAlign: 'center', padding: '1rem', height: '4rem' }}>
        <Select
          mode="multiple"
          placeholder="Select exchanges"
          defaultValue={availableExchanges}
          onChange={this.onExchangesChange}
          style={{ margin: '0 1rem', minWidth: '8rem' }}
        >
          {
            availableExchanges.map(exchange => {
              const exchangeName = EXCHANGE_NAME_MAP_FOR_LOGO(exchange).toLowerCase();
              return (
                <Option key={exchange}>
                  <img
                    src={exchangesImages[exchangeName.toLowerCase()]}
                    alt={exchangeName}
                    title={exchangeName}
                    style={{ height: '1rem', width: '1rem'}}/>
                  &nbsp;{exchange}
                </Option>);
            })
          }
        </Select>
        <Button
          type="primary"
          shape="circle"
          icon="search"
          onClick={() => this.onCreateInterest()}
        />
      </div>
    );
  }

}

const mapStateToProps = state => ({
  referenceData: state.referenceData
});
const mapDispatchToProps = dispatch => ({
  createInterest:
    (interestId, tickerPair, volume, direction, exchanges) =>
      dispatch(interestActions.createInterest(interestId, tickerPair, volume, direction, exchanges)),
});
export default connect(mapStateToProps, mapDispatchToProps)(TradeDetails);
