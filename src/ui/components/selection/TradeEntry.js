import React from 'react';
import { connect } from 'react-redux';
import { Affix } from 'antd';
import Help from '../common/Help';
import Info from '../common/Info';
import Selector from './Selector';
import TradeDetails from './TradeDetails';

class TradeEntry extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { referenceData } = this.props;
    return (
      <div
        style={{ height: '15%', width: '100%', display: 'table' }}>
        <Affix style={{ position: 'absolute', top: '2rem', left: '2rem' }}>
          <Help />
          &nbsp;
          &nbsp;
          <Info />
        </Affix>
        <div
          style={{ display: 'table-cell', verticalAlign: 'middle' }}>
          <Selector />
          {
            referenceData.tickerPair
              ? <TradeDetails />
              : <div style={{ height: '4rem' }}/>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  referenceData: state.referenceData,
  interest: state.interest,
});
export default connect(mapStateToProps)(TradeEntry);
