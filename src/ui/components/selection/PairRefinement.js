import React from 'react';
import { connect } from 'react-redux';

class PairRefinement extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { referenceData } = this.props;
    if (! (referenceData && referenceData.tickerPair)) {
      return (<div>No pair selected</div>);
    }
    return (
      <div>
        { JSON.stringify(referenceData.tickerPair) }
        { JSON.stringify(referenceData.availableExchanges) }
      </div>
    );
  }

}

const mapStateToProps = state => ({
  referenceData: state.referenceData
});
export default connect(mapStateToProps)(PairRefinement);
