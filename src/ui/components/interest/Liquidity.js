import React from 'react';
import { connect } from 'react-redux';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import TradeDisplay from './TradeDisplay';
import Depth from '../common/Depth';
import './Liquidity.css';

class Liquidity extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { interestKey, interest } = this.props;
    const { exchanges, liquidity, direction } = interest[interestKey];
    if (liquidity) {
      const { depth } = liquidity;
      return (
        <Tabs>
          <TabList>
            <Tab>Details</Tab>
            <Tab>Depth</Tab>
          </TabList>
          <TabPanel>
            <TradeDisplay interestKey={interestKey} />
          </TabPanel>
          <TabPanel>
            <Depth depth={depth} direction={direction} exchanges={exchanges}/>
          </TabPanel>
        </Tabs>
      );
    }
  }

}

const mapStateToProps = state => ({
  referenceData: state.referenceData,
  interest: state.interest
});
export default connect(mapStateToProps)(Liquidity);
