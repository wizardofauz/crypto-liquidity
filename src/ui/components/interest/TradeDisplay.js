import React from 'react';
import { connect } from 'react-redux';
import { Table } from 'antd';
import {Decimal} from 'decimal.js';
import coins from '../../images/coins';
import NumberDisplay from '../common/NumberDisplay';

class TradeDisplay extends React.PureComponent {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: '',
        dataIndex: 'step',
        key: 'exchange',
      },
      {
        title: 'Low tier',
        dataIndex: 'min',
        key: 'min',
      },
      {
        title: 'Avg',
        dataIndex: 'average',
        key: 'average',
      },
      {
        title: 'High tier',
        dataIndex: 'max',
        key: 'max',
      }];
  }

  createTree = (fees) => {
    return fees.map(fee => {
      const { summary } = fee;
      let children = [];
      Object
        .keys(fee.summary)
        .filter(value => !['exchange', 'low', 'high'].includes(value))
        .forEach(key => {
          if (fee.summary[key].low && fee.summary[key].high) {
            children.push({
              key: key,
              rowKey: key,
              step: key,
              max: <NumberDisplay number={new Decimal(fee.summary[key].low)} />,
              min: <NumberDisplay number={new Decimal(fee.summary[key].high)} />,
              average: <NumberDisplay
                number={new Decimal(fee.summary[key].low).plus(fee.summary[key].high).dividedBy(2)} />
            });
          }
        });

      const tree = summary;
      tree.key = 'fees.'+summary.exchange;
      tree.rowKey = 'fees.'+summary.exchange;
      tree.step = summary.exchange;
      tree.max = <NumberDisplay number={new Decimal(summary.low)} />;
      tree.min = <NumberDisplay number={new Decimal(summary.high)} />;
      tree.average = <NumberDisplay number={new Decimal(summary.low).plus(summary.high).dividedBy(2)} />;
      if (children.length > 0) {
        tree.children = children;
      }

      return tree;
    });
  };


  render() {
    const { interest, interestKey } = this.props;
    const currentInterest = interest[interestKey];

    const {
      liquidity,
      volume,
    } = currentInterest;

    const { avgPrice, cumulativePrice, trades, fill } = liquidity;
    const currency = currentInterest.tickerPair.right.symbol;
    const currencySymbol = (<img
      src={coins[currency.toLowerCase()]}
      alt='?'
      title={currency}
      style={{ height: '1rem', width: '1rem' }} />);

    const { low, high } = trades
      ? trades
        .map(trade => trade.fees.summary)
        .reduce(({ low, high }, cumul) => {
          return {
            low: low + cumul ? cumul.low : 0,
            high: high + cumul ? cumul.high: 0
          };
        })
      : { low: '?', high: '?' };

    const percentageFill = Math.round(((parseFloat(fill) / parseFloat(volume)) * 100));
    this.columns[0].title = (<div>{currencySymbol} values for a { percentageFill }% fill</div>);

    const feeChildren = this.createTree(trades.map(trade => trade.fees));
    const total = new Decimal(cumulativePrice);
    const lowFees = new Decimal(high);
    const highFees = new Decimal(low);

    const steps = [];
    steps.push({ key: 'avg', rowKey: 'avg', step: 'Avg. Price', average: <NumberDisplay number={avgPrice} /> });
    steps.push({
      key: 'fees',
      rowKey: 'fees',
      step: 'Fees',
      min: <NumberDisplay number={high} />,
      average: <NumberDisplay number={(lowFees.plus(highFees).dividedBy(2))} />,
      max: <NumberDisplay number={low} />,
      children: feeChildren
    });
    steps.push({
      key: 'total',
      rowKey: 'total',
      step: 'Total Price',
      min: <NumberDisplay number={(total.plus(lowFees))} />,
      average: <NumberDisplay number={total.plus(((lowFees.plus(highFees)).dividedBy(2)))} />,
      max: <NumberDisplay number={(total.plus(highFees))} />
    });
    return (
      <Table
        style={{minHeight: '100%'}}
        columns={this.columns}
        dataSource={steps}
        size="small"
        pagination={false}
      />
    )
  }

}

const mapStateToProps = state => ({
  interest: state.interest
});
export default connect(mapStateToProps)(TradeDisplay);
