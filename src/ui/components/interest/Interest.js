import React from 'react';
import { connect } from 'react-redux';
import { Button, Card, Spin, Alert, Popover, InputNumber, Switch, Row } from 'antd';
import TimeAgo from 'react-timeago';
import { EXCHANGE_NAME_MAP_FOR_LOGO } from '../../common/constant';
import { interestActions } from '../../reducers/interest';
import Liquidity from './Liquidity';
import coins from '../../images/coins';
import exchangesImages from '../../images/exchanges';
import Unknown from '../../images/unknown.svg';

import './Interest.css';

class Interest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modifyInterestPopoverVisible: false,
      popoverVolume: null,
      popoverDirection: null,
    };
  }

  componentDidUpdate() {
    const { interest, interestKey } = this.props;
    const currentInterest = interest[interestKey];

    const {
      volume,
      direction,
    } = currentInterest;

    const { popoverVolume, popoverDirection } = this.state;
    if (popoverVolume === null) {
      this.setState({ popoverVolume: volume });
    }
    if (popoverDirection === null) {
      this.setState({ popoverDirection: direction });
    }
  }

  axeInterest = () => {
    const { interestKey, axeInterest } = this.props;
    axeInterest(interestKey);
  };

  modifyInterest = (volume, direction) => {
    const { interestKey, modifyInterest } = this.props;
    modifyInterest(interestKey, volume, direction);
  };

  triggerModifyInterest = () => {
    const { popoverVolume, popoverDirection } = this.state;
    this.modifyInterest(popoverVolume, popoverDirection);
    this.setState({
      modifyInterestPopoverVisible: false,
    });
  };

  handleVisibleChange = (visible) => {
    const { interest, interestKey } = this.props;
    const currentInterest = interest[interestKey];

    const {
      volume,
      direction,
    } = currentInterest;
    this.setState({ modifyInterestPopoverVisible: visible, cache: { volume, direction } });
  };

  renderPopoverContent() {
    let { popoverVolume, popoverDirection } = this.state;

    return (
      <div>
        <Row align='middle' justify='center' style={{ margin: '0.5rem'}}>
          Volume: <InputNumber
            min={1}
            style={{ marginLeft: 16 }}
            value={popoverVolume}
            onChange={_ => this.setState({ popoverVolume: _ })}
          />
        </Row>
        <Row align='middle' justify='center' style={{ margin: '0.5rem'}}>
          BUY &nbsp;
          <Switch
            size="small"
            checked={popoverDirection === 'SELL'}
            onChange={_ => this.setState({ popoverDirection: _ ? 'SELL' : 'BUY' })} />
          &nbsp; SELL
        </Row>
        <Row align='right' justify='right' style={{ margin: '0.5rem'}}>
          <a style={{ float: 'right' }} onClick={this.triggerModifyInterest}>Save</a>
        </Row>
      </div>
    );
  }

  renderTitleBar(tickerPair, volume, direction, exchanges, lastUpdate) {
    const pair = tickerPair
      ? `${tickerPair.left.symbol}/${tickerPair.right.symbol}`
      : '...';
    const details = tickerPair
      ? `${direction} x${volume}`
      : '';

    return (
      <div style={{ whiteSpace: 'no-wrap', overflowX: 'hidden', overflowY: 'hidden' }}>
        { tickerPair
          ? coins[tickerPair.left.symbol.toLowerCase()]
            ? <img
              src={coins[tickerPair.left.symbol.toLowerCase()]}
              alt='?'
              title={tickerPair.left.symbol}
              style={{ height: '2rem', width: '2rem', marginRight: '1rem', marginLeft: '5px' }}/>
            : <img
              src={Unknown}
              alt='?'
              title={tickerPair.left.symbol}
              style={{ height: '2rem', width: '2rem', marginRight: '1rem', marginLeft: '5px' }}/>
          : <div/>
        }
        <h3 style={{ display: 'inline-block' }}>{ pair }</h3>
        <h3 style={{ display: 'inline-block', marginLeft: '1rem', }}>{ details }</h3>
        <Popover
          content={this.renderPopoverContent(volume, direction)}
          title="Modify"
          trigger="click"
          visible={this.state.modifyInterestPopoverVisible}
          onVisibleChange={this.handleVisibleChange}
        >
          <Button
            type="dashed"
            shape="circle"
            icon="edit"
            style={{ display: 'inline-block', marginLeft: '2rem', verticalAlign: 'center' }}
          />
        </Popover>
        <ul style={{ display: 'inline' }}>
          {
            (exchanges || []).map(exchange => {
              const exchangeName = EXCHANGE_NAME_MAP_FOR_LOGO(exchange).toLowerCase();
              return (
                <li key={exchange} style={{ display: 'inline' }} id={exchange}>
                  <img
                    src={exchangesImages[exchangeName.toLowerCase()]}
                    alt={exchangeName}
                    title={exchangeName}
                    style={{ height: '2rem', width: '2rem'}}/>
                </li>
              )
            })
          }
        </ul>
        <h4 style={{ display: 'inline-block', marginLeft: '4rem', verticalAlign: 'center' }}>
          <TimeAgo date={lastUpdate} />
        </h4>
      </div>
    );
  }

  render() {
    const { interest, interestKey } = this.props;
    const currentInterest = interest[interestKey];

    const {
      tickerPair,
      exchanges,
      loading,
      volume,
      direction,
      liquidity,
    } = currentInterest;
    const lastUpdate = Date.now();

    return (
      <Card
        style={{ width: '100%', height: '25rem', minWidth: '46rem' }}
        bodyStyle={{ padding: '5px 5px 5px 5px' }}
        title={this.renderTitleBar(tickerPair, volume, direction, exchanges, lastUpdate)}
        extra={(
          <Button
            type="danger"
            shape="circle"
            icon="close"
            onClick={() => this.axeInterest()} />
        )}>
        {
          loading || !liquidity || liquidity.status === 'PENDING'
            ? (
              <Spin tip="Aggregating orderbooks" style={{ height: '20rem' }}>
                <Alert
                  style={{ height: '20rem' }}
                  type="info"
                />
              </Spin>)
            : <Liquidity interestKey={interestKey} />
        }
      </Card>
    );
  }

}

const mapStateToProps = state => ({
  interest: state.interest
});
const mapDispatchToProps = dispatch => ({
  axeInterest:(interestId) =>
    dispatch(interestActions.axeInterest(interestId)),
  modifyInterest:(interestId, volume, direction) =>
    dispatch(interestActions.modifyInterest(interestId, volume, direction)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Interest);
