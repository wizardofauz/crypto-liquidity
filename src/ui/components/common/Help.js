import React from 'react';
import { connect } from 'react-redux';
import { Icon, Popover, Steps } from 'antd';
import { throttled } from '../../common/util';
import GoogleAnalytics from "react-ga";

const { Step } = Steps;

class Help extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      popoverVisible: false
    }
  }

  /*
  componentDidMount() {
    const listener =
      throttled(() => {
        const scrollTop = window.pageYOffset || window.scrollTop;
        const scrollPercent = scrollTop / window.innerHeight || 0;
        const scrollRelative = (Math.min(scrollPercent, 0.2)) / 0.2;
        this.setState({
          scrollPercent: scrollRelative,
        })
      }, 33);
    window.addEventListener('scroll', listener);
    this.setState({
      listener: listener
    });
  }

  componentWillUnmount() {
    const { listener } = this.state;
    if (listener) {
      window.addEventListener('scroll', listener);
    }
  }
  */

  handleVisibleChange = () => {
    const { popoverVisible } = this.state;
    GoogleAnalytics.event({
      category: 'Help',
      action: 'Clicked'
    });
    this.setState({ popoverVisible: ! popoverVisible });
  };

  hidePopover = () => {
    this.setState({ popoverVisible: false });
  };

  render() {
    const { scrollPercent, popoverVisible } = this.state;
    const { referenceData } = this.props;
    const offset = -4 * scrollPercent;

    const { tickerPair, availableExchanges } = referenceData;
    let stage, error = false;
    if (! tickerPair) {
      stage = 1;
      error = false;
    } else if (tickerPair && tickerPair.invalid) {
      stage = 1;
      error = true;
    } else if (! availableExchanges) {
      stage = 1;
      error = false;
    } else if (availableExchanges.length < 1) {
      stage = 2;
      error = true;
    } else {
      stage = 3;
      error = false;
    }

    return (
      <Popover
        content={
          <div>
            <Steps direction="vertical" size="small" current={ (stage || 1) - 1 } status={error ? 'error' : ''}>
              <Step title="Create a trading pair" description="Choose 2 tickers." />
              <Step title="Choose exchanges" description="From the valid exchanges listed." />
              <Step
                title="Stream!"
                status={ stage >= 2 ? 'finish': 'wait'}
                icon={stage === 3 ? <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />: ''} />
            </Steps>
          </div>
        }
        title="How to use"
        placement="rightTop"
        trigger="click"
        visible={scrollPercent > 0 ? false : popoverVisible}
        onVisibleChange={this.handleVisibleChange}
      >
        <Icon
          type={ popoverVisible ? 'close-circle' : 'question-circle'}
          style={{ fontSize: 24, marginLeft: `${offset}rem` }}
          theme="outlined" />
      </Popover>
    );
  }

}

const mapStateToProps = (state) => ({
  referenceData: state.referenceData,
});
export default connect(mapStateToProps)(Help);
