import React from 'react';
import { connect } from 'react-redux';
import { Icon, Popover, Steps } from 'antd';
import exchangesImages from '../../images/exchanges';
import GoogleAnalytics from "react-ga";

const { Step } = Steps;

class Info extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      popoverVisible: false
    }
  }

  handleVisibleChange = () => {
    const { popoverVisible } = this.state;
    GoogleAnalytics.event({
      category: 'Info',
      action: 'Clicked'
    });
    this.setState({ popoverVisible: ! popoverVisible });
  };

  hidePopover = () => {
    this.setState({ popoverVisible: false });
  };

  render() {
    const { scrollPercent, popoverVisible } = this.state;
    const offset = -4 * scrollPercent;

    return (
      <Popover
        content={
          <div>
            {
              Object.keys(exchangesImages).map(exch => {
                return (<img
                  src={exchangesImages[exch]}
                  key={exch}
                  alt={exch}
                  title={exch}
                  style={{ height: '2rem', width: '2rem', opacity: exch === 'bittrex' ? 0.5 : 1 }}/>)
              })
            }
          </div>
        }
        title="Supported Exchanges"
        placement="rightTop"
        trigger="click"
        visible={scrollPercent > 0 ? false : popoverVisible}
        onVisibleChange={this.handleVisibleChange}
      >
        <Icon
          type={ popoverVisible ? 'close-circle' : 'info-circle'}
          style={{ fontSize: 24, marginLeft: `${offset}rem` }}
          theme="outlined" />
      </Popover>
    );
  }

}

const mapStateToProps = (state) => ({
  referenceData: state.referenceData,
});
export default connect(mapStateToProps)(Info);
