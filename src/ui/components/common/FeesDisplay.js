import React from 'react';
import { connect } from 'react-redux';
import { Table } from 'antd';

const columns = [
  {
    title: 'Exchange',
    dataIndex: 'exchange',
    key: 'exchange',
  },
  {
    title: 'Low-tier fees',
    dataIndex: 'low',
    key: 'low',
  },
  {
    title: 'High-tier fees',
    dataIndex: 'high',
    key: 'high',
  }];

const createTree = (fees) => {
  return fees.map(fee => {
    const { summary } = fee;
    let children = [];
    Object
      .keys(fee.summary)
      .filter(value => !['exchange', 'low', 'high'].includes(value))
      .forEach(key => {
        if (fee.summary[key].low && fee.summary[key].high) {
          children.push({
            key: key,
            rowKey: key,
            exchange: key,
            low: fee.summary[key].low,
            high: fee.summary[key].high
          });
        }
      });

    const tree = summary;
    tree.rowKey = summary.exchange;
    tree.key = summary.exchange;
    if (children.length > 0) {
      tree.children = children;
    }

    return tree;
  });
};

class FeesDisplay extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { trades } = this.props;

    if (! trades) {
      return (<h4>?</h4>);
    }

    const fees = createTree(trades
      .map(trade => trade.fees));
    return (
      <div style={{ minHeight: '100%', paddingBottom: '1rem' }}>
        <Table columns={columns} dataSource={fees} size="small" pagination={false} />
      </div>
    );
  }

}

export default connect()(FeesDisplay);
