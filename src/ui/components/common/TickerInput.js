import React from 'react';
import { Select } from 'antd';
import coins from '../../images/coins';
import Unknown from '../../images/unknown.svg';

const Option = Select.Option;

class TickerInput extends React.PureComponent {
  constructor(props) {
    super(props);
    const tickerSymbols = props.tickers
      .map(ticker => ticker.symbol)
      .filter((val, index, self) => self.indexOf(val) === index)
      .filter(val => Object.keys(coins).includes(val.toLowerCase()))
      .sort()
      .map(ticker => {
        return (
          <Option key={ticker} value={ticker}>
            {
              coins[ticker.toLowerCase()]
                ? <img
                  src={coins[ticker.toLowerCase()]}
                  alt='?'
                  title={ticker}
                  style={{ height: '1rem', width: '1rem' }}/>
                : <img
                  src={Unknown}
                  alt='?'
                  title={ticker}
                  style={{ height: '1rem', width: '1rem' }}/>
            }
            &nbsp;{ticker}
          </Option>
        );
      });
    this.state = {
      onTickerSelected: props.onTickerSelected,
      tickers: props.tickers,
      tickerSymbols: tickerSymbols,
      dataSource: undefined,
    };
    this.inputRef = React.createRef();
  }

  focus = () => {
    this.inputRef.current.focus();
  };

  onFocus = (e) => {
    this.inputRef.current.rcSelect.setOpenState(true, true);
    this.setState({
      editing: true
    });
  };

  onSelect = (value) => {
    const { onTickerSelected } = this.state;
    const { tickers } = this.props;
    if (onTickerSelected) {
      const ticker = tickers.find(entry => entry.symbol === value);
      onTickerSelected(ticker);
      this.setState({
        selected: value
      });
    }
  };

  render() {
    const { tickerSymbols } = this.state;

    return (
      <Select
        showSearch
        optionFilterProp="children"
        dropdownMatchSelectWidth={true}
        ref={this.inputRef}
        style={{ padding: '0 2rem', width: '12rem' }}
        placeholder="Select ticker"
        onChange={this.onSelect}
        onFocus={this.onFocus}
        filterOption={(input, option) => option.key.toLowerCase().includes(input)}
      >
        { tickerSymbols }
      </Select>)
  }

}

export default (TickerInput);
