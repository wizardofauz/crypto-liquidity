import React from 'react';
import NumericLabel from 'react-pretty-numbers';

const numericOption = {
  'justification': 'L',
  'locales': 'en-US',
  'percentage': false,
  'precision': 7,
  'wholenumber': null,
  'commafy': true,
  'shortFormat': true,
  'shortFormatMinValue': 100000,
  'shortFormatPrecision': 1,
};

export default class NumberDisplay extends React.Component {

  constructor() {
    super();
    this.state = {
      lastNumber: undefined
    }
  }

  render() {
    const { number } = this.props;
    const { lastNumber } = this.state;
    let colorCode;
    if (!lastNumber) {
      colorCode = '#373737';
    }
    if (number < lastNumber) {
      colorCode = '#52100b';
    }
    if (number > lastNumber) {
      colorCode = '#1e521d';
    }

    this.lastNumber = number;
    return (
      <NumericLabel params={numericOption} style={{ color: colorCode }}>{ number }</NumericLabel>
    )
  }

}
