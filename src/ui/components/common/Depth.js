import React from 'react';
import {
  XAxis,
  YAxis,
  CartesianGrid,
  Legend,
  BarChart,
  Brush,
  Bar,
  Tooltip, AreaChart, Area,
} from 'recharts';
import {chunkArrayInGroups} from '../../common/util';

const uniqueColorsSet =
  ["#bc7d39",
    "#687dd3",
    "#6fac5d",
    "#944fa1",
    "#b94663"];

const LIQUIDITY_STACKS_CATEGORIES = 10;

export default class Depth extends React.Component {

  constructor() {
    super();
  }

  computeStacks = (depthSide, exchanges) => {
    return Object.keys(depthSide)/*.slice(0, 100)*/.map(key => {
      const options = depthSide[key];
      let exchangeVolumes = {};
      options.forEach(option => {
        const { exchange, volume } = option;
        exchangeVolumes[exchange] = volume;
      });
      const entry = { price: key };
      exchanges.forEach(exchangeKey => {
        entry[exchangeKey] = exchangeVolumes[exchangeKey] || 0;
      });
      return entry;
    });
  };

  categorizeStacks = (stacks, exchanges) => {
    const chunks = chunkArrayInGroups(stacks, (stacks.length / LIQUIDITY_STACKS_CATEGORIES));
    const categories = [];
    for (let chunk of chunks) {
      const firstPrice = chunk[0].price;
      const lastPrice = chunk[chunk.length-1].price;
      const volumeAgg = chunk.reduce((entry, aggregation) => {
        if (!entry || !aggregation)
          return;
        for (let exchange of exchanges) {
          aggregation[exchange] = (aggregation[exchange] || 0) + entry[exchange];
        }
        return aggregation;
      });
      categories.push({
        firstPrice,
        lastPrice,
        ...volumeAgg
      });
    }
    return categories;
  };

  render() {
    const { depth, exchanges, direction } = this.props;
    const buy = depth['BUY'];
    const sell = depth['SELL'];
    const side = (direction === 'BUY'
      ? buy
      : sell);
    let joined = this.categorizeStacks(this.computeStacks(side, exchanges), exchanges);
    if (direction !== 'BUY') {
      joined = joined.reverse();
    }
    return (
      <div style={{ textAlign: 'center' }}>
        <BarChart
          data={joined}
          layout="vertical"
          width={800}
          height={260}
          margin={{ left: 35 }}
          style={{ display: 'inline-block' }}>
          <CartesianGrid strokeDasharray="3 3" />
          <YAxis type="category" dataKey="firstPrice"/>
          <XAxis type="number" />
          <Tooltip/>
          {/*<Brush dataKey='firstPrice' height={30} stroke="#8884d8" />*/}
          <Legend />
          {
            exchanges.map(exchange =>
              (<Bar key={exchange} dataKey={exchange} stackId="a"
                fill={`${uniqueColorsSet[exchanges.indexOf(exchange)]}`} />))
          }
        </BarChart>
      </div>
    );

  }

}
