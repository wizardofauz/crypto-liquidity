import { compose, applyMiddleware } from 'redux';

export default function (middlewares = []) {
  let composeEnhancers = compose;
  if (process.env.NODE_ENV === 'development') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  }
  return composeEnhancers(applyMiddleware(...middlewares));
}
