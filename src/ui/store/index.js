import { createStore, combineReducers } from 'redux';
import statusReducer from '../reducers/status';
import referenceDataReducer from '../reducers/referenceData';
import interestReducer from '../reducers/interest';
import feedbackReducer from '../reducers/feedback';

const rootReducer = combineReducers({
  status: statusReducer,
  referenceData: referenceDataReducer,
  interest: interestReducer,
  feedback: feedbackReducer,
});

const store = createStore(
  rootReducer,
);

export default store;
