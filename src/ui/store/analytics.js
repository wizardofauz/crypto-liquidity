import React from 'react';
import GoogleAnalytics from 'react-ga';

const withTracker = (WrappedComponent, options = {}) => {
  const trackPage = page => {
    GoogleAnalytics.set({
      page,
      ...options,
    });
    GoogleAnalytics.pageview(page, 'trace.ninja');
  };

  const HOC = class extends React.Component {
    constructor() {
      super();
      this.state = {};
    }

    componentDidMount() {
      const page = this.props.location.pathname;
      trackPage(page);
    }
    componentWillReceiveProps(nextProps, nextContext) {
      const currentPage = this.props.location.pathname;
      const nextPage = nextProps.location.pathname;

      if (currentPage !== nextPage) {
        trackPage(nextPage);
      }
    }
    render() {
      return <WrappedComponent {...this.props} />;
    }
  };

  return HOC;
};

export default withTracker;
