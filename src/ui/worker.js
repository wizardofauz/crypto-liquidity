import PrimusClient from './common/PrimusClient';

let primusClient;
const startBroadcast = (data) => {
  const { origin } = data;
  primusClient = new PrimusClient(origin, self, [])
    .then(client => {
      primusClient = client;
    });
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function waitForClient() {
  if (!primusClient) {
    await sleep(1000);
    await waitForClient();
  }
  return primusClient;
}

const getClient = () => {
  return new Promise(resolve => {
    waitForClient().then(resolve);
  });
};

self.onmessage = (msg) => {
  const { type, data } = typeof msg.data === 'string' ? { type: msg.data } : msg.data;
  switch (type) {
    case 'open':
      self.postMessage('connected');
      break;
    case 'startSession':
      startBroadcast(data);
      break;
    case 'sendData':
      getClient().then(client => client.write(data));
      break;
    case 'finish':
      getClient().then(client => client.destroy());
      break;
    case 'pairs':
    case 'interest':
    case 'axe_interest':
    case 'tickerPair':
      self.postMessage({ type, data });
      break;
    default:
      console.log('Unknown message in worker: ', msg.data); // eslint-disable-line no-console
  }
};

