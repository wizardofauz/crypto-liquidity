import btc from './btc.svg';
import eth from './eth.svg';
import xrp from './xrp.svg';
import bch from './bch.svg';
import eos from './eos.svg';
import ltc from './ltc.svg';
import iota from './iota.svg';
import neo from './neo.svg';
import etc from './etc.svg';
import mgo from './mgo.svg';
import zec from './zec.svg';
import xmr from './xmr.svg';
import dash from './dash.svg';
import zrx from './zrx.svg';
import omg from './omg.svg';
import btg from './btg.svg';
import trx from './trx.svg';
import xlm from './xlm.svg';
import bat from './bat.svg';
import qtum from './qtum.svg';
import san from './san.svg';
import lym from './lym.svg';
import elf from './elf.svg';
import iot from './iot.svg';
import xvg from './xvg.svg';
import gnt from './gnt.svg';
import rep from './rep.svg';
import dth from './dth.svg';
import mana from './mana.svg';
import tkn from './tkn.svg';
import snt from './snt.svg';
import req from './req.svg';
import ess from './ess.svg';
import rlc from './rlc.svg';
import rcn from './rcn.svg';
import rdn from './rdn.svg';
import mln from './mln.svg';
import lrc from './lrc.svg';
import dgb from './dgb.svg';
import wtc from './wtc.svg';
import cnd from './cnd.svg';
import bnt from './bnt.svg';
import ant from './ant.svg';
import bcc from './bcc.svg';
import cfi from './cfi.svg';
import ven from './ven.svg';
import stk from './stk.svg';
import usdt from './usdt.svg';
import ada from './ada.svg';
import xzc from './xzc.svg';
import powr from './powr.svg';
import gxs from './gxs.svg';
import poly from './poly.svg';
import dcr from './dcr.svg';
import storj from './storj.svg';
import cvc from './cvc.svg';
import xem from './xem.svg';
import pay from './pay.svg';
import bts from './bts.svg';
import link from './link.svg';
import mco from './mco.svg';
import waves from './waves.svg';
import ast from './ast.svg';
import lsk from './lsk.svg';
import steem from './steem.svg';
import ae from './ae.svg';
import adx from './adx.svg';
import qash from './qash.svg';
import salt from './salt.svg';
import gas from './gas.svg';
import dgd from './dgd.svg';
import eng from './eng.svg';
import icx from './icx.svg';
import nano from './nano.svg';
import gtc from './gtc.svg';
import bcd from './bcd.svg';
import ardr from './ardr.svg';
import grs from './grs.svg';
import hsr from './hsr.svg';
import dadi from './dadi.svg';
import vib from './vib.svg';
import ace from './ace.svg';
import stc from './stc.svg';
import mda from './mda.svg';
import zen from './zen.svg';
import sc from './sc.svg';
import ark from './ark.svg';
import bcn from './bcn.svg';
import ppt from './ppt.svg';
import dent from './dent.svg';
import lend from './lend.svg';
import ubtc from './ubtc.svg';
import mth from './mth.svg';
import sub from './sub.svg';
import sngls from './sngls.svg';
import nxt from './nxt.svg';
import icn from './icn.svg';
import ctr from './ctr.svg';
import rvn from './rvn.svg';
import bnb from './bnb.svg';
import aion from './aion.svg';
import ethos from './ethos.svg';
import strat from './strat.svg';
import kmd from './kmd.svg';
import amb from './amb.svg';
import pivx from './pivx.svg';
import nxs from './nxs.svg';
import nav from './nav.svg';
import wings from './wings.svg';
import cloak from './cloak.svg';
import sys from './sys.svg';
import trig from './trig.svg';
export default {
  btc,
  eth,
  xrp,
  bch,
  eos,
  ltc,
  iota,
  neo,
  etc,
  mgo,
  zec,
  xmr,
  dash,
  zrx,
  omg,
  btg,
  trx,
  xlm,
  bat,
  qtum,
  san,
  lym,
  elf,
  iot,
  xvg,
  gnt,
  rep,
  dth,
  mana,
  tkn,
  snt,
  req,
  ess,
  rlc,
  rcn,
  rdn,
  mln,
  lrc,
  dgb,
  wtc,
  cnd,
  bnt,
  ant,
  bcc,
  cfi,
  ven,
  stk,
  usdt,
  ada,
  xzc,
  powr,
  gxs,
  poly,
  dcr,
  storj,
  cvc,
  xem,
  pay,
  bts,
  link,
  mco,
  waves,
  ast,
  lsk,
  steem,
  ae,
  adx,
  qash,
  salt,
  gas,
  dgd,
  eng,
  icx,
  nano,
  gtc,
  bcd,
  ardr,
  grs,
  hsr,
  dadi,
  vib,
  ace,
  stc,
  mda,
  zen,
  sc,
  ark,
  bcn,
  ppt,
  dent,
  lend,
  ubtc,
  mth,
  sub,
  sngls,
  nxt,
  icn,
  ctr,
  rvn,
  bnb,
  aion,
  ethos,
  strat,
  kmd,
  amb,
  pivx,
  nxs,
  nav,
  wings,
  cloak,
  sys,
  trig,
};
