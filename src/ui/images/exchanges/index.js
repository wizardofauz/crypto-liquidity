import binance from './binance.svg';
import bitfinex from './bitfinex.svg';
import huobi from './huobi.svg';
import okex from './okex.svg';
import bittrex from './bittrex.svg';
import bitflyer from './bitflyer.svg';
import hitbtc from './hitbtc.svg';

export default {
  binance,
  bitfinex,
  huobi,
  okex,
  bittrex,
  bitflyer,
  hitbtc,
};
