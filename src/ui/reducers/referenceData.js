import { getPair, getTickers } from '../api/realtime';

const initialState = {
  tickers: [],
  pairs: [],
  loadingTickers: false,
  tickerPair: undefined,
  availableExchanges: [],
  loadingPair: false,
};

export const referenceDataActions = {
  FETCH_PAIR: 'FETCH_PAIR',
  fetchPair:
    (tickerLeft, tickerRight) => ({ type: referenceDataActions.FETCH_PAIR, payload: {tickerLeft, tickerRight}}),
  FETCH_TICKERS: 'FETCH_TICKERS',
  fetchTickers: () => ({ type: referenceDataActions.FETCH_TICKERS }),
};

function referenceDataReducer(state = initialState, action) {
  switch (action.type) {
    case referenceDataActions.FETCH_TICKERS: {
      getTickers();
      return ({
        ...state,
        ...{ loadingTickers: true }
      });
    }
    case 'pairs': {
      const { tickers, pairs } = action.payload;
      const validTickers = tickers.filter(ticker => (pairs[ticker.symbol] && pairs[ticker.symbol].length > 0));
      return {
        ...state,
        ...{
          loadingTickers: false,
          tickers: validTickers,
          pairs
        }
      };
    }
    case referenceDataActions.FETCH_PAIR: {
      const { tickerLeft, tickerRight } = action.payload;
      getPair(tickerLeft, tickerRight);
      return ({
        ...state,
        ...{ loadingPair: true }
      });
    }
    case 'tickerPair': {
      const { tickerPair, availableExchanges } = action.payload;
      return ({
        ...state,
        ...{
          loadingPair: false,
          tickerPair,
          availableExchanges
        }
      });
    }
    default:
      return state;
  }
}

export default referenceDataReducer;
