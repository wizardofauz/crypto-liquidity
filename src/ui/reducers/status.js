import { CONNECTED, CONNECTING, DISCONNECTED, RECONNECTING, UNABLE_TO_RECONNECT } from '../common/constant';

const initialState = {
  status: undefined,
};

export const statusAction = {
  PUSH_STATUS: 'PUSH_STATUS',
  pushAction: (status) => ({ type: statusAction.PUSH_STATUS, payload: status }),
};

function statusReducer(state = initialState, action) {
  switch (action.type) {
    case statusAction.PUSH_STATUS:
      let status = action.payload.data;
      const { status: oldStatus } = state;

      if (oldStatus === DISCONNECTED && status === CONNECTING) {
        status = RECONNECTING;
      }
      if (oldStatus === RECONNECTING && (status === CONNECTING || status === DISCONNECTED)) {
        status = RECONNECTING;
      }
      if (oldStatus === UNABLE_TO_RECONNECT && (status === DISCONNECTED)) {
        status = UNABLE_TO_RECONNECT;
      }

      return {
        ...state,
        ...{ status },
      };
    default:
      return state;
  }
}

export default statusReducer;
