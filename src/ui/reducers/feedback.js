import { sendFeedback } from "../api/realtime";

const initialState = {
  feedback: undefined,
};

export const feedbackActions = {
  SEND_FEEDBACK: 'SEND_FEEDBACK',
  sendFeedback: (payload) => ({ type: feedbackActions.SEND_FEEDBACK, payload }),
};

function feedbackReducer(state = initialState, action) {
  switch (action.type) {
    case feedbackActions.SEND_FEEDBACK:
      sendFeedback(action.payload);
      return state;
    default:
      return state;
  }
}

export default feedbackReducer;
