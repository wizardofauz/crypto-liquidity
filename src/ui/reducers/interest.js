import { createInterest, axeInterest, modifyInterest } from '../api/realtime';

const initialState = {};

export const interestActions = {
  RECREATE_INTERESTS: 'RECREATE_INTERESTS',
  recreateInterests: () => ({
    type: interestActions.RECREATE_INTERESTS,
    payload: {},
  }),
  CREATE_INTEREST: 'CREATE_INTEREST',
  createInterest:
    (interestId, tickerPair, volume, direction, exchanges) => ({
      type: interestActions.CREATE_INTEREST,
      payload: { interestId, tickerPair, volume, direction, exchanges },
    }),
  AXE_INTEREST: 'AXE_INTEREST',
  axeInterest:
    (interestId) => ({
      type: interestActions.AXE_INTEREST,
      payload: { interestId },
    }),
  MODIFY_INTEREST: 'MODIFY_INTEREST',
  modifyInterest:
    (interestId, volume, direction) => ({
      type: interestActions.MODIFY_INTEREST,
      payload: { interestId, volume, direction },
    }),
};

function interestReducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    case interestActions.RECREATE_INTERESTS: {
      const interests = Object.keys(state);
      interests
        .map(interest => state[interest].memento)
        .forEach(memento => {
          console.log(memento);
          const { interestId, tickerPair, volume, direction, exchanges } = memento;
          createInterest(interestId, tickerPair, volume, direction, exchanges);
          state[interestId].loading = true;
        });
      return state;
    }
    case interestActions.AXE_INTEREST: {
      const { interestId } = action.payload;
      axeInterest(interestId);
      return state;
    }
    case 'axe_interest': {
      const { interestId } = action.payload;
      delete state[interestId];
      return ({
        ...state,
      });
    }
    case interestActions.MODIFY_INTEREST: {
      const { interestId, volume, direction } = action.payload;
      const newMemento = {
        ...state[interestId].memento,
        ...{ interestId, volume, direction }
      };
      modifyInterest(interestId, volume, direction);
      state[interestId].memento = newMemento;
      return state;
    }
    case 'modify_interest': {
      const { interest } = action.payload;
      newState = {};
      newState[interest.interestId] = {
        ...state[interest.interestId],
        ...interest,
      };
      return ({
        ...state,
        ...newState,
      });
    }
    case interestActions.CREATE_INTEREST: {
      const { interestId, tickerPair, volume, direction, exchanges } = action.payload;
      createInterest(interestId, tickerPair, volume, direction, exchanges);
      const newInterest = {};
      newInterest[action.payload.interestId] =
        {
          loading: true,
          memento: { interestId, tickerPair, volume, direction, exchanges }
        };
      return ({
        ...state,
        ...newInterest,
      });
    }
    case 'interest': {
      const { interest } = action.payload;
      newState = {};
      newState[interest.interestId] = {
        ...state[interest.interestId],
        ...{ loading: false },
        ...interest,
      };
      return ({
        ...state,
        ...newState,
      });
    }
    case 'liquidity': {
      const { liquidity, interestId } = action.payload;
      newState = {};
      newState[interestId] = {
        ...state[interestId],
        ...{ liquidity },
        ...{ loading: false },
      };
      return ({
        ...state,
        ...newState,
      });
    }
    default:
      return state;
  }
}

export default interestReducer;
