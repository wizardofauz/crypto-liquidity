import { createBrowserHistory } from 'history';

const history = createBrowserHistory({ hashType: 'slash' });

export default function () {
  return history;
}
