import React from 'react';
import { connect } from 'react-redux';
import { Col, Icon, notification, Row, Spin, List } from 'antd';
import {CONNECTED, RECONNECTING, STATUS_SEVERITY_MAP, UNABLE_TO_RECONNECT} from '../common/constant';
import { referenceDataActions } from '../reducers/referenceData';
import { feedbackActions} from '../reducers/feedback';
import {interestActions} from '../reducers/interest';
import Interest from '../components/interest/Interest';
import TradeEntry from '../components/selection/TradeEntry';

class App extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
    this.slackFeedbackRef = React.createRef();
  }

  notifyStreamStatus = (status) => {
    const type = STATUS_SEVERITY_MAP[status];
    notification[type]({
      key: 'status',
      message: 'Stream status',
      description: status === RECONNECTING
        ? (
          <div><Icon type="loading" style={{ fontSize: 24 }} spin />&nbsp;&nbsp;{ status }</div>
        )
        : status,
      duration: (status === RECONNECTING || status === UNABLE_TO_RECONNECT) ? 0 : 3
    });
  };

  componentWillMount() {
    const { tickers, fetchTickers } = this.props;
    if (!tickers) {
      fetchTickers();
    }
  }

  componentDidUpdate(prevProps) {
    const { status } = this.props.status;
    const { status: oldStatus } = prevProps.status;
    if (oldStatus === RECONNECTING && status === CONNECTED) {
      // We've reconnected, recreate the interests
      this.props.recreateInterests();
    }
    if (status !== oldStatus) {
      this.notifyStreamStatus(status);
    }
  }

  renderLoading = () => {
    return (
      <div
        style={{ height: '100%', width: '100%', margin: '0 auto' }}>
        <Row gutter={0} align='middle' style={{ height: '100%', width: '100%' }}>
          <Col span={24} style={{ height: '100%', width: '100%' }}>
            <Spin style={{ display: 'block', marginTop: '20%' }}
              tip="Loading..." />
          </Col>
        </Row>
      </div>
    );
  };

  render() {
    const { interest, status, referenceData } = this.props;
    if (referenceData.loadingTickers) {
      return this.renderLoading();
    }
    if (status.status === UNABLE_TO_RECONNECT) {
      return (
        <div
          style={{
            height: '100%',
            width: '100%',
            display: 'table',
            position: 'absolute'
          }}>
          <div style={{ display: 'table-cell', verticalAlign: 'middle' }}>
            <div style={{ marginLeft: 'auto', marginRight: 'auto', width: '300px' }}>
              <div style={{ display: 'inline-grid', textAlign: 'center' }}>
                <Icon type="fire" theme="twoTone" twoToneColor="#df2b26" style={{ fontSize: 36 }}/>
                <br/>
                <h3>Ooops, we're kaput!</h3>
              </div>
            </div>
          </div>
        </div>
      )
    }

    return (
      <div
        style={{ height: '99vh', width: '99vw' }}>
        <TradeEntry />
        <List
          grid={{ gutter: 16, xs: 1, sm: 1, md: 1, lg: 1, xl: 1, xxl: 2 }}
          dataSource={ Object.keys(interest) }
          renderItem={
            interestKey => {
              return (
                <List.Item>
                  <Interest key={interestKey} interestKey={interestKey}/>
                </List.Item>
              );
            }
          }
        />
        <div
          style={{ position: 'fixed', bottom: 0, left: 0, width: '100%', height: '1.5rem', background: '#b1e8d0' }}>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', fontFamily: 'monospace', sansSerif: true }}>
            © { new Date().getUTCFullYear() } cryptotab.dev - &nbsp;
            <div>
              <a className="donate-with-crypto"
                 href="https://commerce.coinbase.com/checkout/82261135-2911-42ac-94ba-26fac371979f">
                <span>Help keep the servers running</span>
              </a>
              <script src="https://commerce.coinbase.com/v1/checkout.js?version=201807">
              </script>
            </div>
            &nbsp; - { __COMMIT_HASH__ }
          </div>
        </div>
      </div>
    );
  }

}
const mapStateToProps = (state) => ({
  interest: state.interest,
  status: state.status,
  referenceData: state.referenceData,
});
const mapDispatchToProps = dispatch => ({
  fetchTickers: () => dispatch(referenceDataActions.fetchTickers()),
  sendFeedback: (payload) => dispatch(feedbackActions.sendFeedback(payload)),
  recreateInterests: () => dispatch(interestActions.recreateInterests()),
});
export default connect(mapStateToProps, mapDispatchToProps)(App);
