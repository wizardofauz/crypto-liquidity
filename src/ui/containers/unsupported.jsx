import React from 'react';
import background from '../images/what-the-hex-dark.png';

export default class Unsupported extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { browser } = this.props;
    return (
      <div
        style={{
          backgroundImage: `url(${background})`,
          height: '100%',
          width: '100%',
          display: 'table',
          position: 'absolute'
        }}>
        <div style={{ display: 'table-cell', verticalAlign: 'middle' }}>
          <div style={{ marginLeft: 'auto', marginRight: 'auto', width: '300px' }}>
            <h4 style={{ color: '#df2b26' }}>Our support for { browser } is still in draft mode...</h4>
            <h3 style={{ color: '#df2b26' }}>Please use Chrome or Firefox!</h3>
          </div>
        </div>
      </div>
    );
  }
}
