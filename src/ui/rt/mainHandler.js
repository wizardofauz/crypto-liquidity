import { statusAction } from '../reducers/status';

const getBasePath = ({ pathname }) => (pathname.substring(0, pathname.lastIndexOf('/') + 1));
const getOrigin = ({ location: { origin, pathname } }) => (`${origin}${getBasePath({ pathname })}`);

export default function (store, worker) {
  return (event) => {
    const { type, ...data } = typeof event.data === 'string' ? { type: event.data } : event.data.data;
    switch (type) {
      case 'connected':
        worker.postMessage({
          type: 'startSession',
          data: {
            origin: getOrigin(window),
          },
        });
        break;
      case 'primus-status':
        store.dispatch(statusAction.pushAction(data));
        break;
      case 'pairs':
      case 'tickerPair':
      case 'interest':
      case 'liquidity':
      case 'axe_interest':
      default:
        store.dispatch({
          type: type,
          payload: data
        });
        break;
    }
  };
}
