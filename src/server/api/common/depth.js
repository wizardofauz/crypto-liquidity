import { EventEmitter } from 'events';
import Logger from 'common/utils/logger';

const logger = new Logger('depth');

export default class Depth extends EventEmitter {

  constructor(symbol, book) {
    super();
    this.symbol = symbol;
    this.book = book;
  }

  getFullDepth = (exchangeKeys) => {
    logger.info(`Generating depth for ${this.symbol} across`, exchangeKeys);
    if (!exchangeKeys || exchangeKeys.length === 0) {
      exchangeKeys = this.book.getAvailableFeedsExchanges();
    }
    const exchangeData = exchangeKeys.map(exchange => {
      return { exchange, data: this.book.getCache(exchange) }
    });

    const sidedDepths = ['BUY', 'SELL'].map(direction => {
      const consolidationWithExchanges = {};
      exchangeData.forEach(entry => {
        const { exchange, data } = entry;
        if (!data)
          return;
        const interestSet = direction === 'BUY' ? data.asks : data.bids;
        Object.keys(interestSet)
          .forEach(key => {
            if (!key) return;
            if (!consolidationWithExchanges[key]) {
              consolidationWithExchanges[key] = [];
            }
            consolidationWithExchanges[key].push({ exchange, volume: interestSet[key] });
          });
      });

      let sortedConsolidationWithExchanges = {};
      const sortedPrices = Object.keys(consolidationWithExchanges)
        .sort(function (a, b) {
          return direction === 'BUY'
            ? parseFloat(a) - parseFloat(b)
            : parseFloat(b) - parseFloat(a);
        });
      sortedPrices.forEach(price => {
        sortedConsolidationWithExchanges[price] = consolidationWithExchanges[price];
      });
      return sortedConsolidationWithExchanges;
    });

    const ret = {};
    ret['BUY'] = sidedDepths[0];
    ret['SELL'] = sidedDepths[1];
    return ret;
  }

}
