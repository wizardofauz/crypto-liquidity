export const sortBids = (bids, max = Infinity, baseValue = false) => {
  let object = {}, count = 0;
  let cache = bids;
  let sorted = Object.keys(cache).sort(function (a, b) {
    return parseFloat(b) - parseFloat(a)
  });
  let cumulative = 0;
  for (let price of sorted) {
    if (baseValue === 'cumulative') {
      cumulative += parseFloat(cache[price]);
      object[price] = cumulative;
    } else if (!baseValue) object[price] = parseFloat(cache[price]);
    else object[price] = parseFloat((cache[price] * parseFloat(price)).toFixed(8));
    if (++count >= max) break;
  }
  return object;
};

export const sortAsks = (asks, max = Infinity, baseValue = false) => {
  let object = {}, count = 0;
  let cache = asks;
  let sorted = Object.keys(cache).sort(function (a, b) {
    return parseFloat(a) - parseFloat(b);
  });
  let cumulative = 0;
  for (let price of sorted) {
    if (baseValue === 'cumulative') {
      cumulative += parseFloat(cache[price]);
      object[price] = cumulative;
    } else if (!baseValue) object[price] = parseFloat(cache[price]);
    else object[price] = parseFloat((cache[price] * parseFloat(price)).toFixed(8));
    if (++count >= max) break;
  }
  return object;
};

export const first = (object) => {
  return Object.keys(object).shift();
};

export const last = (object) => {
  return Object.keys(object).pop();
};

export const find = (arr, test, ctx) => {
  let result = null;
  arr.some((el, i) => {
    return test.call(ctx, el, i, arr) ? ((result = el), true) : false;
  });
  return result;
};


/**
 * Simple is object check.
 * @param item
 * @returns {boolean}
 */
export function isObject(item) {
  return (item && typeof item === 'object' && !Array.isArray(item) && item !== null);
}

/**
 * Deep merge two depth objects.
 * @param target
 * @param source
 */
export function mergeDeep(target, source) {
  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        mergeDeep(target[key], source[key]);
      } else {
        if (target[key] && source[key] && typeof(target[key]) === 'number' && typeof(source[key]) === 'number') {
          Object.assign(target, { [key]: source[key] + target[key] });
        } else Object.assign(target, { [key]: source[key] });
      }
    }
  }
  return target;
}

export function throttled(fn, delay) {
  let lastCall = 0;
  return function (...args) {
    const now = (new Date).getTime();
    if (now - lastCall < delay) {
      return;
    }
    lastCall = now;
    return fn(...args);
  }
}
