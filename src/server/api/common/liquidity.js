import { EventEmitter } from 'events';
import { Decimal } from 'decimal.js';
import { BOOK_UPDATE } from './constant';
import { throttled } from './util';
import Logger from 'common/utils/logger';
import Depth from 'api/common/depth';

const logger = new Logger('liquidity');

export default class Liquidity extends EventEmitter {

  constructor(symbol, book, ctx) {
    super();
    this.symbol = symbol;
    this.book = book;
    this.ctx = ctx;
    this.depthCache = new Depth(symbol, book);
  }

  priceAtDepthPerExchange = (depth, volumeRequested, direction, exchangeKeys) => {
    const sortedConsolidationWithExchanges = depth[direction];

    const pricePoints = Object.keys(sortedConsolidationWithExchanges);
    if (pricePoints.length <= 0)
      return { status: 'PENDING' };

    let fill = new Decimal(0), maxReached = false, cumulativePrice = 0, priceIndex = 0, tradeStructure = {};
    exchangeKeys.forEach(k => tradeStructure[k] = []);
    while(fill < volumeRequested && !maxReached && priceIndex < pricePoints.length) {
      const price = pricePoints[priceIndex++];
      const entries = sortedConsolidationWithExchanges[price];
      const currentPotentialFill = entries.map(e => e.volume).reduce((r, l) => new Decimal(r).plus(new Decimal(l)));
      const tranche = (volumeRequested - fill);
      let take = Math.min(tranche, currentPotentialFill);

      entries.forEach(entry => {
        const localTake = Math.min(entry.volume, take);
        tradeStructure[entry.exchange].push({ price, volume: localTake });
        take -= localTake;
        fill = fill.plus(new Decimal(localTake));
        cumulativePrice = new Decimal(cumulativePrice).plus(new Decimal(localTake).times(new Decimal(price)));
      });

      if (priceIndex >= pricePoints.length) {
        maxReached = true;
      }
    }

    const trades = Object.keys(tradeStructure).map(exchange => {
      const exchangeOrders = tradeStructure[exchange];
      let aggregatedPrice = 0, volume = 0;
      exchangeOrders.forEach(order => {
        aggregatedPrice = new Decimal(aggregatedPrice).plus(new Decimal(order.volume).times(new Decimal(order.price)));
        volume = new Decimal(volume).plus(new Decimal(order.volume));
      });
      const avgPrice = new Decimal(aggregatedPrice).dividedBy(volume);
      return {
        exchange: exchange.toString(),
        volume: volume.toString(),
        aggregatedPrice: aggregatedPrice.toString(),
        avgPrice: avgPrice.toString() };
    }).filter(entry => new Decimal(entry.volume) > 0);

    const avgPrice = new Decimal(cumulativePrice).dividedBy(fill);
    return {
      fill: fill.toString(),
      maxReached,
      cumulativePrice: cumulativePrice.toString(),
      avgPrice: avgPrice.toString(),
      trades: trades
    };
  };

  addInterest = (stream, throttle, volumeRequested, direction, exchangeKeys) => {
    if (! exchangeKeys || exchangeKeys.length === 0) {
      exchangeKeys = this.book.getAvailableFeedsExchanges();
    }
    const that = this;
    this.publisher = throttled(() => {
      const depth = that.depthCache.getFullDepth(exchangeKeys);
      const interestLiquidity = that.priceAtDepthPerExchange(depth, volumeRequested, direction, exchangeKeys);
      stream({ ...interestLiquidity, depth, timestamp: Date.now() });
    }, throttle);
    this.book.on(BOOK_UPDATE, this.publisher);
  };

  removeInterest = () => {
    logger.info(this.ctx, `Deleting liquidity batch calculation for ${this.symbol}`);
    this.book.removeListener(BOOK_UPDATE, this.publisher);
    delete this.publisher;
  };

}
