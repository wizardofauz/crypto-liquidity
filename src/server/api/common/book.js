import { EventEmitter } from 'events';
import { sortAsks, sortBids, first } from './util';
import { BOOK_UPDATE } from './constant';
import Logger from 'common/utils/logger';

const logger = new Logger('book');
export default class Book extends EventEmitter {

  constructor(symbol) {
    super();
    this.symbol = symbol;
    this.cache = {};
    this.topOfBook = {};
  }

  getAvailableFeedsExchanges() {
    return Object.keys(this.cache);
  }

  getCache(exchangeKey) {
    return this.cache[exchangeKey];
  }

  processBids(exchangeKey, depth) {
    // console.log('bids', depth.bids);
    let bids = sortBids(depth.bids);

    this.cache[exchangeKey].bids = bids;
    this.topOfBook[exchangeKey].bid = first(bids);
    this.topOfBook[exchangeKey].bidSize = depth.bids[first(bids)];

    // console.log(this.topOfBook);
    if (! this.topOfBook[exchangeKey].bid
      ||! this.topOfBook[exchangeKey].bidSize) {
      logger.warn('Empty top of book (bid)', exchangeKey, depth);
    }
    this.emit(BOOK_UPDATE, exchangeKey);
  }

  processAsks(exchangeKey, depth) {
    // console.log('asks', depth.asks);
    let asks = sortAsks(depth.asks);

    this.cache[exchangeKey].asks = asks;
    this.topOfBook[exchangeKey].ask = first(asks);
    this.topOfBook[exchangeKey].askSize = depth.asks[first(asks)];

    // console.log(this.topOfBook);
    if (! this.topOfBook[exchangeKey].ask
      ||! this.topOfBook[exchangeKey].askSize) {
      logger.warn('Empty top of book (ask)', exchangeKey, depth);
    }
    this.emit(BOOK_UPDATE, exchangeKey);
  }

  /**
   * Expect data as such
   * asks: {
   *   '0.26667000': 1188.3,
   *   '0.26700000': 19452.8,
   *   ...
   * },
   * bids: {
   *   '0.26667000': 1188.3,
   *   '0.26700000': 19452.8,
   *   '0.26740000': 2195.8,
   *   '0.26741000': 4443.5,
   *   ...
   * }
   * @param exchangeKey - the Exchange nomenclature
   * @param messageTransform - a function to be applied over the original message received
   * to turn it into the expected format and correct number of arguments
   * @param dataTransform - a function to be applied over the original object received
   * to turn it into the expected format
   * @return {Function} A feed processing function
   */
  addFeed(exchangeKey, messageTransform, dataTransform) {
    this.cache[exchangeKey] = { bids: {}, asks: {} };
    this.topOfBook[exchangeKey] = { bid: undefined, ask: undefined, bidSize: undefined, askSize: undefined };
    return (...params) => {
      const { depth } = messageTransform
        ? messageTransform(params)
        : { depth: params.depth };
      // console.log(exchangeKey, depth);
      const data = dataTransform
        ? dataTransform(depth)
        : depth;
      if (data) {
        if (data.bids && Object.keys(data.bids).length > 0) {
          this.processBids(exchangeKey, data);
        }
        if (data.asks && Object.keys(data.asks).length > 0) {
          this.processAsks(exchangeKey, data);
        }
      }
      logger.info(this.symbol, `Processed feed update for ${exchangeKey}`);
      logger.debug(this.symbol, JSON.stringify(this.cache, undefined, 2));
    };
  }

}
