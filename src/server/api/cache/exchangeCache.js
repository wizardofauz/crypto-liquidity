import fs from 'fs';
import util from 'util';
import request from 'request-promise-native';
import Logger from 'common/utils/logger';

const logger = new Logger('exchange-cache');
fs.readFileAsync = util.promisify(fs.readFile);
fs.writeFileAsync = util.promisify(fs.writeFile);

export default class ExchangeCache {

  static exchanges = {};
  static tickers = {};

  static async registerExchange(exchange) {
    logger.info(`Registering new exchange ${exchange.name}`);
    if (! this.exchanges[exchange.name]) {
      this.exchanges[exchange.name] = exchange;
    }
    let fetchSymbolsPromise;
    const cacheFileName = `etc/CACHE_SYMBOLS_${exchange.name}.json`;
    if (fs.existsSync(cacheFileName)) {
      logger.info(`Loading CACHE data ${cacheFileName}`);
      fetchSymbolsPromise = fs.readFileAsync(cacheFileName, 'utf-8')
        .then(content => JSON.parse(content));
    } else {
      logger.info(`FETCHing data as ${cacheFileName} inexistent`);
      const requestOptions = {
        uri: 'https://rest.coinapi.io/v1/symbols',
        qs: {
          market: exchange.name,
          filter_symbol_id: `${exchange.name.toUpperCase()}_SPOT`,
        },
        headers: {
          'User-Agent': 'Sneaky, sneaky!',
          'X-CoinAPI-Key': '1EC0EA16-0929-49D9-9A26-7896B4CD0F49'
        },
        json: true,
      };
      fetchSymbolsPromise = request(requestOptions)
        .then(pairs => fs.writeFileAsync(cacheFileName, JSON.stringify(pairs, undefined, 2), 'utf-8')
          .then(() => pairs));
    }
    return await fetchSymbolsPromise
      .then(pairs => {
        this.exchanges[exchange.name].supportedPairs = pairs;
        logger.info(`${exchange.name} supports ${pairs.length} symbols`);
        for(let pair of pairs) {
          const { asset_id_base, asset_id_quote } = pair;
          if (! this.tickers[asset_id_base]) {
            this.tickers[asset_id_base] = {};
          }
          if (! this.tickers[asset_id_base][asset_id_quote]) {
            this.tickers[asset_id_base][asset_id_quote] = { supportedExchanges: []};
          }
          this.tickers[asset_id_base][asset_id_quote].supportedExchanges.push(this.exchanges[exchange.name]);
        }
      })
      .then(() => this.exchanges[exchange.name]);
  }

  static getExchangePairs(exchange) {
    return this.exchanges[exchange.name].supportedPairs;
  }

  static getExchange(exchangeKey) {
    return this.exchanges[exchangeKey]
  }

  static getAllRegisteredPairs(tickers) {
    const ret = {};
    const selected = Object
      .keys(this.tickers)
      .filter(ticker => tickers.includes(ticker));
    for(let current of selected) {
      ret[current] = Object.keys(this.tickers[current]);
    }
    return ret;
  }

  static getExchangesForTicker(ticker) {
    const { left, right } = ticker;
    if (! this.tickers || ! this.tickers[left.symbol] || ! this.tickers[left.symbol][right.symbol]) {
      logger.error('Unsupported pair across our exchanges: ' + left.symbol + '/' + right.symbol);
      return [];
    }
    return this.tickers[left.symbol][right.symbol].supportedExchanges;
  }

}
