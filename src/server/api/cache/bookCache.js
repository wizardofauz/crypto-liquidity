import Book from '../common/book';

export default class BookCache {
  static books = [];

  static registerBook(left, right) {
    this.books[left][right] = new Book(`${left}${right}`);
  }

  static getBook(leftSymbol, rightSymbol) {
    if(leftSymbol === rightSymbol)
      throw new Error(`Can't create a pair for ${leftSymbol} === ${rightSymbol}`);
    if(! this.books[leftSymbol]) {
      this.books[leftSymbol] = [];
    }
    if(! this.books[leftSymbol][rightSymbol]) {
      this.registerBook(leftSymbol, rightSymbol);
    }
    return this.books[leftSymbol][rightSymbol];
  }

}
