import Logger from 'common/utils/logger';
import config from 'common/utils/config';
import Interest from '../interest';
import { LIQUIDITY_UPDATE } from '../common/constant';
import ExchangeCache from '../cache/exchangeCache';
import {forwardFeedback} from '../user/feedback';

const logger = new Logger('realtime');
let instance;

const ELAPSED_DC_TO_REMOVAL = config.web_service.elapsed_dc_to_removal;
const TOP_TICKERS = config.web_service.top_tickers;
const sparksToInterests = {};
const sparksToIP = {};

const sparkAttributes = spark => {
  return { sparkId: spark.id, ip: sparksToIP[spark.id] ? sparksToIP[spark.id].ip : 'removed' };
};

export default class RealtimeHandler {
  constructor(primus) {
    if (!instance && primus) {
      instance = this;
    }
    this.uiLink = primus;
    this.uiLink.on('connection', this.onConnect);
    this.uiLink.on('disconnection', this.onDisconnect);
    this.uiLink.on('error', this.onError);
    this.uiLink.on('reconnect', this.onReconnect);

    this.reconnections = {};
    return instance;
  }

  addEventListeners = (spark) => {
    const eventTypes = ['outgoing::reconnect', 'reconnect scheduled', 'reconnected', 'reconnect timeout',
      /* 'connection', 'disconnection', 'error', 'reconnect', */ 'reconnect failed', 'timeout', 'outgoing::open',
      'incoming::open', 'open', 'destroy', 'incoming::error', /* 'incoming::data', 'outgoing::data', */ 'incoming::end',
      'outgoing::end', 'end', 'close', 'initialised', 'incoming::ping', 'outgoing::ping', 'incoming::pong',
      'outgoing::pong', /* 'heartbeat', */ 'online', 'offline', 'log', /* 'readyStateChange', */ 'outgoing::url'];

    eventTypes.forEach(eventType => (
      spark.on(eventType, data => {
        logger.info(sparkAttributes(spark), `${eventType}: ${data || ''}`);
      })
    ));
  };

  broadcast = (data) => {
    this.lastBroadcast = data;
    this.uiLink.write(data);
  };

  onConnect = (spark) => {
    if (this.reconnections[spark.id]) {
      logger.info(sparkAttributes(spark), 'Reconnection is being attempted');
      clearTimeout(this.reconnections[spark.id]);
    } else {
      sparksToIP[spark.id] = spark.address;
    }

    spark.on('data', data => {
      const { type, ...other } = data;
      logger.info(sparkAttributes(spark), 'Received data', data);
      this.onData(type, other, spark);
    });

    spark.write({ type: 'late-joiner', data: this.lastBroadcast });
    this.addEventListeners(spark);
  };
  onError = (spark, error) => {
    logger.error(sparkAttributes(spark), error);
  };
  onReconnect = (spark) => {
    logger.warn(sparkAttributes(spark), 'Reconnecting');
  };
  onDisconnect = (spark) => {
    logger.info(sparkAttributes(spark), `Disconnection noticed`);
    logger.info(sparkAttributes(spark), `Delaying disconnection by 10 seconds`);
    this.reconnections[spark.id] = setTimeout(() => this.removeUser(spark), ELAPSED_DC_TO_REMOVAL);
  };

  removeUser = async (spark) => {
    if (sparksToInterests[spark.id]) {
      const { interests } = sparksToInterests[spark.id];
      Object.keys(interests).forEach(interestKey => {
        const interest = interests[interestKey];
        interest.destroy();
      });
      delete sparksToInterests[spark.id];
    }
    if (sparksToIP[spark.id]) {
      delete sparksToIP[spark.id];
    }
    if (this.reconnections[spark.id])
      delete this.reconnections[spark.id];
  };

  onData(type, data, spark) {
    switch (type) {
      case 'pairs': {
        const allowedTickers = Object.keys(ExchangeCache.tickers)/*.slice(0, TOP_TICKERS)*/;
        const pairs = ExchangeCache.getAllRegisteredPairs(allowedTickers);
        spark.write(
          {
            type: 'pairs',
            tickers: allowedTickers.map(symbol => ({ symbol })),
            pairs,
          });
        break;
      }
      case 'tickerPair': {
        const { tickerLeft, tickerRight } = data.data;
        try {
          const tickerPair = { left: { symbol: tickerLeft.symbol }, right: { symbol: tickerRight.symbol }};
          const availableExchanges = ExchangeCache
            .getExchangesForTicker(tickerPair)
            .map(_ => _.name);
          logger.info(`Sending ${JSON.stringify(tickerPair)} trading on ${availableExchanges}`);
          spark.write({ type: 'tickerPair', tickerPair, availableExchanges });
        } catch(err) {
          logger.error('Unsupported pair across our exchanges: ' + tickerLeft.symbol + '/' + tickerRight.symbol);
          spark.write({ type: 'tickerPair', tickerPair: { invalid: true }, availableExchanges: [] });
        }
        break;
      }
      case 'interest': {
        const { interestId, tickerPair: pair, volume, direction, exchanges } = data.data;
        const interest = {
          interestId, tickerPair: pair, volume, direction, exchanges
        };
        try {
          const interestSubscription = new Interest(interest, sparkAttributes(spark));
          interestSubscription
            .on(LIQUIDITY_UPDATE, update => spark.write({ type: 'liquidity', interestId, liquidity: update }));
          if (!sparksToInterests[spark.id]) {
            sparksToInterests[spark.id] = { interests: {} };
          }
          sparksToInterests[spark.id].interests[interestId] = (interestSubscription);
        } catch (err) {
          logger.error(sparkAttributes(spark), 'LIQUIDITY', err);
        }

        logger.info(sparkAttributes(spark), `Sending ${JSON.stringify(interest)}`);
        spark.write({ type: 'interest', interest });
        break;
      }
      case 'axe_interest': {
        const { interestId } = data.data;
        logger.info(sparkAttributes(spark), `Deleting interest ${interestId}`);
        if (sparksToInterests[spark.id]) {
          const interest = sparksToInterests[spark.id].interests[interestId];
          if (interest) {
            interest.destroy();
            delete sparksToInterests[spark.id].interests[interestId];
            logger.info(sparkAttributes(spark), `Interest ${interestId} removed`);
            spark.write({ type: 'axe_interest', interestId });
          }
        }
        break;
      }
      case 'modify_interest': {
        const { interestId, volume, direction } = data.data;
        logger.info(sparkAttributes(spark),
          `Modifying interest ${interestId}: volume ${volume}, direction ${direction}`);
        if (sparksToInterests[spark.id]) {
          const interest = sparksToInterests[spark.id].interests[interestId];
          if (interest) {
            if (volume) {
              interest.changeVolume(volume);
            }
            if (direction) {
              interest.changeDirection(direction);
            }
            logger.info(sparkAttributes(spark), `Interest ${interestId} modified`);
            const interestSubset = {
              interestId,
              tickerPair: interest.tickerPair,
              volume: interest.volume,
              direction: interest.direction,
              exchanges: interest.exchanges
            };
            spark.write({ type: 'modify_interest', interest: interestSubset });
          }
        }
        break;
      }
      case 'feedback': {
        logger.info(sparkAttributes(spark), 'Registering feedback');
        forwardFeedback(data.data)
          .then(ret => spark.write({ type: 'feedback', event: 'SUCCESS' }));
        break;
      }
      default:
        break;
    }
  }
}
