import Primus from 'primus';
import http from 'http';
import path from 'path';

import Logger from '../../common/utils/logger';
import config from '../../common/utils/config';

const logger = new Logger('ws-primus');

let instance;
let prefix;
if (prefix && prefix[0] !== '/') {
  prefix = `/${prefix}`;
}

const attachPrimus = (app, opts) => {
  const defaultOptions = {
    transformer: 'websockets',
    pathname: '/rt',
    iknowhttpsisbetter: true,
    parser: 'msgpack',
    compression: true,
    pingInterval: 10000,
    maxLength: 1024 * 1024 * 2, // 2Mb
  };

  // const options = { ...defaultOptions, ...opts };
  const options = Object.assign({}, defaultOptions, opts);
  const transformers = {
    browserchannel: 'browserchannel',
    websockets: 'websockets',
    engineio: 'engine.io',
    faye: 'faye',
    sockj: 'sockjs',
    uws: 'uws',
  };

  const transformer = transformers[options.transformer];
  const httpServer = http.createServer(app.callback());
  const primus = new Primus(httpServer, options);
  instance = primus;

  logger.info(`Primus spawned and attached to '${options.pathname}' route`);
  if (options.benchmarking) logger.info('Primus works is in BENCHMARKING mode');
  logger.info(`Primus transformer is: '${transformer}'`);

  // TODO: Make this less hacky
  // if(process.env.NODE_ENV !== "test") {
  //  logger.info(`Generating PRIMUS library at ${config.ws.assetsDirectory + '/primus.js'}`);
  primus.save(path.resolve(`${config.ws.assetsDirectory}/primus.js`));
  // }

  return {
    server: httpServer,
    primus,
  };
};

export const getInstance = () => {
  if (!instance) {
    throw new Error('Primus is not instantiated');
  } else {
    return instance;
  }
};

export default attachPrimus;
