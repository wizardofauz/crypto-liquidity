import debug from 'debug';
import Koa from 'koa';
import enforceHttps from 'koa-sslify';
import Logger from 'common/utils/logger';
import config from 'common/utils/config';
import {
  loggingLayer,
  initialLayer,
  apiLayer,
  errorLayer,
  timingLayer,
  assetsLayer,
} from 'common/middlewares';
import routes from './routes';

const app = new Koa();
const logger = new Logger('api-server');

if (config.web_service.enforceHttps) {
  app.use(enforceHttps({
    trustProtoHeader: true
  }));
}
timingLayer(app);
loggingLayer(app, logger);
initialLayer(app);

const start = new Date();
assetsLayer(app);
logger.info(`Serving static assets took ${new Date() - start} ms`);

const router = apiLayer(app, routes);
router.stack.map(layer => logger.info(`Created route ${layer.path}`));
errorLayer(app);

// istanbul ignore next
app.on('error', (error) => {
  debug('error')(error);
});

export default app;
