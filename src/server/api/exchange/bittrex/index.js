import bittrex from 'node-bittrex-api';
import Logger from 'common/utils/logger';
const subscriptions = {};

const logger = new Logger('bittrex-wrapper');

bittrex.options = {
  // 'apikey' : API_KEY,
  // 'apisecret' : API_SECRET,
  verbose: true,
  websockets: {
    onConnect: function() {
      logger.info('Bittrex exchange connected');
    },
  },
};

export const getTicker = (ticker) => {
  const { left, right } = ticker;
  return `${left.symbol.toUpperCase()}-${right.symbol.toUpperCase()}`;
};

export default {

  createSubscription: (tickerPair, book) => {
    // throw new Error('Not implemented');
    const symbol = getTicker(tickerPair);

    if (subscriptions[symbol]) {
      logger.info('Found cached bittrex subscription for ', symbol);
      subscriptions[symbol].subscriptionCount++;
    } else {
      logger.info('Creating bittrex subscription for ', symbol);
      // book.addFeed('bittrex', params => )
      subscriptions[symbol] = 1;
      bittrex.getorderbook( { market : symbol, depth : 100, type : 'both' }, params => {
        console.log(params);
        return { symbol, depth: params };
      });
    }
  },

  removeSubscription: (tickerPair) => {
    const symbol = getTicker(tickerPair);
    logger.info('Removing bittrex subscription for ', symbol);
    subscriptions[symbol].subscriptionCount--;
    if (subscriptions[symbol].subscriptionCount <= 0) {
      logger.info('No more subscribers, destroying bittrex subscription for ', symbol);
      const { endpoint } = subscriptions[symbol];
      // endpoint.close();
      delete subscriptions[symbol];
    }
  }

}
