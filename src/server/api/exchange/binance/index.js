import binance from 'node-binance-api';
import Logger from 'common/utils/logger';
import config from 'common/utils/config';

const logger = new Logger('binance-wrapper');

export const getTicker = (ticker) => {
  const { left, right } = ticker;
  return `${left.symbol.toUpperCase()}${right.symbol.toUpperCase()}`;
};

const api = binance().options(config.exchanges.binance.options);

const subscriptions = {};

export default {
  createSubscription: (tickerPair, book) => {
    const symbol = getTicker(tickerPair);
    logger.info('Requesting binance subscription for ', symbol);
    if (subscriptions[symbol])
    {
      logger.info('Found cached binance subscription for ', symbol);
      subscriptions[symbol].subscriptionCount++;
    } else {
      logger.info('Creating binance subscription for ', symbol);
      const endpoint = api.websockets.depthCache([symbol], book.addFeed('binance', params => {
        return { symbol: params[0], depth: params[1] };
      }));
      subscriptions[symbol] = {
        endpoint,
        subscriptionCount: 1,
      };
    }
  },

  removeSubscription: (tickerPair) => {
    const symbol = getTicker(tickerPair);
    logger.info('Removing binance subscription for ', symbol);
    subscriptions[symbol].subscriptionCount--;
    if (subscriptions[symbol].subscriptionCount <= 0) {
      logger.info('No more subscribers, destroying binance subscription for ', symbol);
      const { endpoint } = subscriptions[symbol];
      api.websockets.terminate(endpoint);
      delete subscriptions[symbol];
    }
  }
}
