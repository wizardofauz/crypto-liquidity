import { Decimal } from 'decimal.js';

const schedule = {
  'non-bnb': {
    maker: {
      general: { multiplier: 0.001, text: '0.10%' },
      vip1: { multiplier: 0.0009, text: '0.09%' },
      vip2: { multiplier: 0.0008, text: '0.08%' },
      vip3: { multiplier: 0.0007, text: '0.07%' },
      vip4: { multiplier: 0.0006, text: '0.06%' },
      vip5: { multiplier: 0.0005, text: '0.05%' },
      vip6: { multiplier: 0.0004, text: '0.04%' },
      vip7: { multiplier: 0.0003, text: '0.03%' },
      vip8: { multiplier: 0.0002, text: '0.02%' }
    },
    taker: {
      general: { multiplier: 0.001, text: '0.10%' },
      vip1: { multiplier: 0.001, text: '0.10%' },
      vip2: { multiplier: 0.001, text: '0.10%' },
      vip3: { multiplier: 0.0009, text: '0.09%' },
      vip4: { multiplier: 0.0008, text: '0.08%' },
      vip5: { multiplier: 0.0007, text: '0.07%' },
      vip6: { multiplier: 0.0006, text: '0.06%' },
      vip7: { multiplier: 0.0005, text: '0.05%' },
      vip8: { multiplier: 0.0004, text: '0.04%' }
    }
  },
  bnb: {
    maker: {
      general: { multiplier: 0.00075, text: '0.0750%' },
      vip1: { multiplier: 0.000675, text: '0.0675%' },
      vip2: { multiplier: 0.0006, text: '0.0600%' },
      vip3: { multiplier: 0.000525, text: '0.0525%' },
      vip4: { multiplier: 0.00045, text: '0.0450%' },
      vip5: { multiplier: 0.000375, text: '0.0375%' },
      vip6: { multiplier: 0.0003, text: '0.0300%' },
      vip7: { multiplier: 0.000225, text: '0.0225%' },
      vip8: { multiplier: 0.000150, text: '0.0150%' }
    },
    taker: {
      general: { multiplier: 0.00075, text: '0.0750%' },
      vip1: { multiplier: 0.00075, text: '0.0750%' },
      vip2: { multiplier: 0.00075, text: '0.0750%' },
      vip3: { multiplier: 0.000675, text: '0.0675%' },
      vip4: { multiplier: 0.0006, text: '0.0600%' },
      vip5: { multiplier: 0.000525, text: '0.0525%' },
      vip6: { multiplier: 0.00045, text: '0.0450%' },
      vip7: { multiplier: 0.000375, text: '0.0375%' },
      vip8: { multiplier: 0.0003, text: '0.0300%' },
    }
  }
};

export const calculateFees = (order) => {
  const fees = { bnb: {}, 'non-bnb': {} };
  fees.bnb.maker = {};
  Object.keys(schedule.bnb.maker).map(tier => {
    const template = schedule.bnb.maker[tier];
    fees.bnb.maker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });
  fees.bnb.taker = {};
  Object.keys(schedule.bnb.taker).map(tier => {
    const template = schedule.bnb.taker[tier];
    fees.bnb.taker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });
  fees['non-bnb'].maker = {};
  Object.keys(schedule['non-bnb'].maker).map(tier => {
    const template = schedule['non-bnb'].maker[tier];
    fees['non-bnb'].maker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });
  fees['non-bnb'].taker = {};
  Object.keys(schedule['non-bnb'].taker).map(tier => {
    const template = schedule['non-bnb'].taker[tier];
    fees['non-bnb'].taker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });

  fees.summary = {
    exchange: 'binance',
    bnb: { low: fees.bnb.maker['general'].toString(), high: fees.bnb.maker['vip8'].toString() },
    'non-bnb': { low: fees['non-bnb'].maker['general'].toString(), high: fees['non-bnb'].maker['vip8'].toString() },
    low: fees['non-bnb'].maker['general'].toString(),
    high: fees.bnb.maker['vip8'].toString()
  };
  return fees;
};
