import WebSocket from 'ws';
import Logger from 'common/utils/logger';
import config from 'common/utils/config';
const subscriptions = {};

const logger = new Logger('bitfinex-wrapper');

export const getTicker = (ticker) => {
  const { left, right } = ticker;
  return `t${left.symbol.toUpperCase()}${right.symbol.toUpperCase()}`;
};

export default {

  createSubscription: (tickerPair, book) => {
    const symbol = getTicker(tickerPair);
    const ws = new WebSocket(config.exchanges.bitfinex.url);

    if (subscriptions[symbol]) {
      logger.info('Found cached bitfinex subscription for ', symbol);
      subscriptions[symbol].subscriptionCount++;
    } else {
      logger.info('Creating bitfinex subscription for ', symbol);
      let channelSubscriptionId;
      const orderCache = [];
      ws.onmessage = book.addFeed('bitfinex',
        params => {
          return { depth: JSON.parse(params[0].data) };
        },
        depth => {
          if (depth.chanId) {
            logger.info('Confirmed channel subscription from bitfinex', depth.chanId);
            channelSubscriptionId = depth.chanId;
          }

          if (channelSubscriptionId && depth[0] === channelSubscriptionId) {
            let updates = depth[1];
            if (updates === 'hb')
              return;
            if (typeof updates[0] === 'number') {
              updates = [ updates ];
            }
            for(let update of updates) {
              const orderId = update[0];
              const price = update[1];
              const volume = update[2];
              if (! orderId)
                continue;
              if (price === 0) {
                // Removal of the order in the cache
                delete orderCache[orderId];
              } else {
                if (orderCache[orderId]) {
                  orderCache[orderId] = { price, volume: orderCache[orderId].volume + volume };
                } else {
                  orderCache[orderId] = { price, volume: volume };
                }
              }
            }

            const unsortedBids = {}, unsortedAsks = {};
            Object.keys(orderCache).forEach(id => {
              const object = orderCache[id];
              if (! object.price)
                return;
              if(object.volume < 0) {
                unsortedBids[object.price] =
                  (unsortedBids[object.price] ? unsortedBids[object.price] : 0) + Math.abs(object.volume);
              } else {
                unsortedAsks[object.price] =
                  (unsortedAsks[object.price] ? unsortedAsks[object.price] : 0) + Math.abs(object.volume);
              }
            });

            return { bids: unsortedBids, asks: unsortedAsks };
          }
        });
      ws.onopen = () => {
        // API keys setup here (See "Authenticated Channels")
        ws.send(JSON.stringify({
          event: 'subscribe',
          channel: 'book',
          freq: 'F1',
          prec: 'R0',
          len: 100,
          symbol: symbol,
        }));
      };
      subscriptions[symbol] = {
        endpoint: ws,
        subscriptionCount: 1,
      };
    }
  },

  removeSubscription: (tickerPair) => {
    const symbol = getTicker(tickerPair);
    logger.info('Removing bitfinex subscription for ', symbol);
    subscriptions[symbol].subscriptionCount--;
    if (subscriptions[symbol].subscriptionCount <= 0) {
      logger.info('No more subscribers, destroying bitfinex subscription for ', symbol);
      const { endpoint } = subscriptions[symbol];
      endpoint.close();
      delete subscriptions[symbol];
    }
  }

}
