import { Decimal } from 'decimal.js';

const schedule = {
  maker: {
    tier1: { multiplier: 0.001, text: '0.100%' },
    tier2: { multiplier: 0.0008, text: '0.080%' },
    tier3: { multiplier: 0.0006, text: '0.060%' },
    tier4: { multiplier: 0.0004, text: '0.040%' },
    tier5: { multiplier: 0.0002, text: '0.020%' },
    tier6: { multiplier: 0, text: '0.000%' },
    tier7: { multiplier: 0, text: '0.000%' },
    tier8: { multiplier: 0, text: '0.000%' },
    tier9: { multiplier: 0, text: '0.000%' },
    tier10: { multiplier: 0, text: '0.000%' },
    tier11: { multiplier: 0, text: '0.000%' },
    tier12: { multiplier: 0, text: '0.000%' },
    tier13: { multiplier: 0, text: '0.000%' },
    tier14: { multiplier: 0, text: '0.000%' },
    tier15: { multiplier: 0, text: '0.000%' },
    tier16: { multiplier: 0, text: '0.000%' }
  },
  taker: {
    tier1: { multiplier: 0.002, text: '0.200%' },
    tier2: { multiplier: 0.002, text: '0.200%' },
    tier3: { multiplier: 0.002, text: '0.200%' },
    tier4: { multiplier: 0.002, text: '0.200%' },
    tier5: { multiplier: 0.002, text: '0.200%' },
    tier6: { multiplier: 0.002, text: '0.200%' },
    tier7: { multiplier: 0.0018, text: '0.180%' },
    tier8: { multiplier: 0.0016, text: '0.160%' },
    tier9: { multiplier: 0.0014, text: '0.140%' },
    tier10: { multiplier: 0.0012, text: '0.120%' },
    tier11: { multiplier: 0.0010, text: '0.100%' },
    tier12: { multiplier: 0.0009, text: '0.090%' },
    tier13: { multiplier: 0.00085, text: '0.085%' },
    tier14: { multiplier: 0.00075, text: '0.075%' },
    tier15: { multiplier: 0.00060, text: '0.060%' },
    tier16: { multiplier: 0.00055, text: '0.055%' }
  }
};

export const calculateFees = (order) => {
  const fees = {};
  fees.maker = {};
  Object.keys(schedule.maker).forEach(tier => {
    const template = schedule.maker[tier];
    fees.maker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });
  fees.taker = {};
  Object.keys(schedule.taker).map(tier => {
    const template = schedule.taker[tier];
    fees.taker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });

  fees.summary = {
    exchange: 'bitfinex',
    low: fees.maker['tier1'].toString(),
    high: fees.maker['tier16'].toString()
  };
  return fees;
};
