import binance from './binance';
import okex from './okex';
import bitfinex from './bitfinex';
import huobipro from './huobi';
import hitbtc from './hitbtc';
// import bittrex from './bittrex';
import bitflyer from './bitflyer';

export default {
  binance,
  okex,
  bitfinex,
  huobipro,
  bitflyer,
  hitbtc,
}
