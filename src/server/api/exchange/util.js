import { calculateFees as binance } from './binance/fees';
import { calculateFees as okex } from './okex/fees';
import { calculateFees as bitfinex } from './bitfinex/fees';
import { calculateFees as huobipro } from './huobi/fees';
import { calculateFees as bitflyer } from './bitflyer/fees';
import { calculateFees as hitbtc } from './hitbtc/fees';

export const calculateFees = (orders) => {
  if (! orders || orders.length === 0) {
    return [];
  }
  const fees = orders.map(order => {
    switch (order.exchange) {
      case 'okex':
        order.fees = okex(order);
        break;
      case 'binance':
        order.fees = binance(order);
        break;
      case 'bitfinex':
        order.fees = bitfinex(order);
        break;
      case 'huobipro':
        order.fees = huobipro(order);
        break;
      case 'bitflyer':
        order.fees = bitflyer(order);
        break;
      case 'hitbtc':
        order.fees = hitbtc(order);
        break;
      default:
        order.fees = { exchange: order.exchange, error: 'Unknown exchange fee schedule'};
    }
  });
  return fees;
};
