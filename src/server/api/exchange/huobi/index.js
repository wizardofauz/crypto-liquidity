import WebSocket from 'ws';
import pako from 'pako';
import Logger from 'common/utils/logger';
import config from 'common/utils/config';
const subscriptions = {};

const logger = new Logger('huobi-wrapper');

export const getTicker = (ticker) => {
  const { left, right } = ticker;
  return `${left.symbol.toLowerCase()}${right.symbol.toLowerCase()}`;
};

export default {

  createSubscription: (tickerPair, book) => {
    const symbol = getTicker(tickerPair);

    if (subscriptions[symbol]) {
      logger.info('Found cached huobi subscription for ', symbol);
      subscriptions[symbol].subscriptionCount++;
    } else {
      logger.info('Creating huobi subscription for ', symbol);
      const ws = new WebSocket(config.exchanges.huobi.url);
      ws.on('message', book.addFeed('huobipro',
        params => {
          if (! params)
            return;
          const text = pako.inflate(params[0], {
            to: 'string'
          });
          const msg = JSON.parse(text);
          return { depth: msg };
        },
        depth => {
          if (! depth)
            return;

          if (depth.ping) {
            logger.info('Responding to heartbeat request', depth.ping);
            ws.send(JSON.stringify({
              pong: depth.ping
            }));
            return;
          }

          if (! depth.tick) {
            return;
          }

          let updates = depth.tick;
          const unsortedBids = {}, unsortedAsks = {};
          for(let update of updates.bids) {
            const price = update[0];
            const volume = update[1];
            if (price === 0 || volume === 0) {
              continue;
            } else {
              unsortedBids[price] = volume;
            }
          }
          for(let update of updates.asks) {
            const price = update[0];
            const volume = update[1];
            if (price === 0 || volume === 0) {
              continue;
            } else {
              unsortedAsks[price] = volume;
            }
          }

          return { bids: unsortedBids, asks: unsortedAsks };
        })
      );
      ws.on('open', () => {
        logger.info('API to huobi open, subscribing');
        ws.send(JSON.stringify({
          "sub": `market.${symbol}.depth.step0`,
          "id": `${symbol}`
        }));
      });
      subscriptions[symbol] = {
        endpoint: ws,
        subscriptionCount: 1,
      };
    }
  },

  removeSubscription: (tickerPair) => {
    const symbol = getTicker(tickerPair);
    logger.info('Removing huobi subscription for ', symbol);
    subscriptions[symbol].subscriptionCount--;
    if (subscriptions[symbol].subscriptionCount <= 0) {
      logger.info('No more subscribers, destroying huobi subscription for ', symbol);
      const { endpoint } = subscriptions[symbol];
      endpoint.close();
      delete subscriptions[symbol];
    }
  }

}
