import { Decimal } from 'decimal.js';

const schedule = {
  maker: {
    general: { multiplier: 0.002, text: '0.20%' },
    vip1: { multiplier: 0.0018, text: '0.18%' },
    vip2: { multiplier: 0.0016, text: '0.16%' },
    vip3: { multiplier: 0.0014, text: '0.14%' },
    vip4: { multiplier: 0.0012, text: '0.12%' },
    vip5: { multiplier: 0.0010, text: '0.10%' },
  },
  taker: {
    general: { multiplier: 0.002, text: '0.20%' },
    vip1: { multiplier: 0.0018, text: '0.18%' },
    vip2: { multiplier: 0.0016, text: '0.16%' },
    vip3: { multiplier: 0.0014, text: '0.14%' },
    vip4: { multiplier: 0.0012, text: '0.12%' },
    vip5: { multiplier: 0.0010, text: '0.10%' },
  }
};

export const calculateFees = (order) => {
  const fees = {};
  fees.maker = {};
  Object.keys(schedule.maker).forEach(tier => {
    const template = schedule.maker[tier];
    fees.maker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });
  fees.taker = {};
  Object.keys(schedule.taker).map(tier => {
    const template = schedule.taker[tier];
    fees.taker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });

  fees.summary = {
    exchange: 'huobipro',
    low: fees.maker['general'].toString(),
    high: fees.maker['vip5'].toString()
  };
  return fees;
};
