import io from 'socket.io-client';
import Logger from 'common/utils/logger';
import config from 'common/utils/config';
const subscriptions = {};

const logger = new Logger('bitflyer-wrapper');

export const getTicker = (ticker) => {
  const { left, right } = ticker;
  return `lightning_board_snapshot_${left.symbol.toUpperCase()}_${right.symbol.toUpperCase()}`;
};

export default {

  createSubscription: (tickerPair, book) => {
    const symbol = getTicker(tickerPair);

    if (subscriptions[symbol]) {
      logger.info('Found cached bitflyer subscription for ', symbol);
      subscriptions[symbol].subscriptionCount++;
    } else {
      logger.info('Creating bitflyer subscription for ', symbol);
      const socket = io(config.exchanges.bitflyer.url, { transports: ['websocket'] });
      socket.on('connect', () => {
        socket.emit('subscribe', symbol);
      });
      const handler = book.addFeed('bitflyer',
        params => {
          return { depth: params[0] }
        },
        depth => {
          let bids = {};
          (depth.bids ? depth.bids : []).forEach(current => {
            bids[current.price] = current.size;
          });
          let asks = {};
          (depth.asks ? depth.asks : []).forEach(current => {
            asks[current.price] = current.size;
          });
          return { bids, asks };
        });
      socket.on(symbol, message => {
        handler(message);
      });

      subscriptions[symbol] = { subscriptionCount: 1, endpoint: socket };
    }
  },

  removeSubscription: (tickerPair) => {
    const symbol = getTicker(tickerPair);
    logger.info('Removing bitflyer subscription for ', symbol);
    subscriptions[symbol].subscriptionCount--;
    if (subscriptions[symbol].subscriptionCount <= 0) {
      logger.info('No more subscribers, destroying bitflyer subscription for ', symbol);
      const { endpoint } = subscriptions[symbol];
      endpoint.disconnect();
      delete subscriptions[symbol];
    }
  }

}
