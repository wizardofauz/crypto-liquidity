import WebSocket from 'ws';
import pako from 'pako';
import Logger from 'common/utils/logger';
import config from 'common/utils/config';

const logger = new Logger('okex-wrapper');

export const getTicker = (ticker) => {
  const { left, right } = ticker;
  return `${left.symbol.toLowerCase()}/${right.symbol.toLowerCase()}`;
};

export class Okex {
  constructor() {
    this.websocketUri = config.exchanges.okex.url;
    this.connect();
  }

  connect() {
    this.socket = new WebSocket(this.websocketUri);
  }

  addSubscriptionDepth(pair_names) {
    this.addSubscription(pair_names, 'depth')
  }

  addSubscriptionTicker(pair_names) {
    this.addSubscription(pair_names, 'ticker')
  }

  addSubscriptionDeals(pair_names) {
    this.addSubscription(pair_names, 'deals')
  }

  addSubscriptionKline(pair_names, kline) {
    //1min, 3min, 5min, 15min, 30min, 1hour, 2hour, 4hour, 6hour, 12hour, day, 3day, week
    this.addSubscription(pair_names, 'kline_'+kline)
  }

  addSubscription(pair_names, type) {
    if (type.indexOf('depth') != -1 || type == 'ticker' || type == 'deals' || type.indexOf('kline') != -1) {
      if (this.socket.readyState != this.socket.OPEN) {
        this.connect();
      }
      this.socket.on('open', () => {
        for (let i = 0; i < pair_names.length; i++) {
          let pair_name = pair_names[i].replace('/', '_').toLowerCase();
          let subscription = {
            event: 'addChannel',
            channel: 'ok_sub_spot_' + pair_name + '_' + type,
          };
          this.socket.send(JSON.stringify(subscription))
        }
      });
    }
  }

  terminate() {
    if (this.socket.readyState == this.socket.OPEN && this.socket.readyState != this.socket.CONNECTING) {
      this.socket.terminate();
    }
  }

  onMessage(callback) {
    this.socket.on('message', data => {
      if (typeof callback === 'function') {
        const string = pako.inflateRaw(data, {to: 'string'});
        return callback(JSON.parse(string));
      }
    });
    this.reconnect(callback);
  }

  reconnect(callback) {
    this.socket.on('close', () => {
      setTimeout(() => {
        this.connect();
        this.onMessage(callback);
      }, 3000);
    });
  }
}

const subscriptions = {};

export default {

  createSubscription: (tickerPair, book) => {
    const symbol = getTicker(tickerPair);
    if (subscriptions[symbol]) {
      logger.info('Found cached okex subscription for ', symbol);
      subscriptions[symbol].subscriptionCount++;
    } else {
      logger.info('Creating okex subscription for ', symbol);
      const okexApi = new Okex();
      okexApi.addSubscriptionDepth([symbol]);
      okexApi.onMessage(book.addFeed('okex',
        params => {
          return { symbol: symbol, depth: params[0] };
        },
        depth => {
          if (!depth)
            return;
          // console.log('okex', depth);
          const asksPreformat = depth[0].data.asks;
          const bidsPreformat = depth[0].data.bids;
          let unsortedAsks,
            unsortedBids;
          if (asksPreformat) {
            unsortedAsks = {};
            asksPreformat.forEach(tuple => {
              unsortedAsks[tuple[0]] = tuple[1];
            });
          }
          if (bidsPreformat) {
            unsortedBids = {};
            bidsPreformat.forEach(tuple => {
              unsortedBids[tuple[0]] = tuple[1];
            });
          }
          return { bids: unsortedBids, asks: unsortedAsks };
        })
      );
      subscriptions[symbol] = {
        endpoint: okexApi,
        subscriptionCount: 1,
      };
    }
  },

  removeSubscription: (tickerPair) => {
    const symbol = getTicker(tickerPair);
    logger.info('Removing okex subscription for ', symbol);
    subscriptions[symbol].subscriptionCount--;
    if (subscriptions[symbol].subscriptionCount <= 0) {
      logger.info('No more subscribers, destroying okex subscription for ', symbol);
      const { endpoint } = subscriptions[symbol];
      endpoint.terminate();
      delete subscriptions[symbol];
    }
  }

}
