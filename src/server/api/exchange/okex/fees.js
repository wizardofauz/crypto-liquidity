import { Decimal } from 'decimal.js';

const schedule = {
  maker: {
    tier1: { multiplier: 0.001, text: '0.10%' },
    tier2: { multiplier: 0.0009, text: '0.09%' },
    tier3: { multiplier: 0.0008, text: '0.08%' },
    tier4: { multiplier: 0.0007, text: '0.07%' },
    tier5: { multiplier: 0.0006, text: '0.06%' },
    tier6: { multiplier: 0.0005, text: '0.05%' },
    tier7: { multiplier: 0.0003, text: '0.03%' },
    tier8: { multiplier: 0.0002, text: '0.02%' }
  },
  taker: {
    tier1: { multiplier: 0.0015, text: '0.15%' },
    tier2: { multiplier: 0.0014, text: '0.14%' },
    tier3: { multiplier: 0.0013, text: '0.13%' },
    tier4: { multiplier: 0.0012, text: '0.12%' },
    tier5: { multiplier: 0.001, text: '0.10%' },
    tier6: { multiplier: 0.0008, text: '0.08%' },
    tier7: { multiplier: 0.0006, text: '0.06%' },
    tier8: { multiplier: 0.0005, text: '0.05%' }
  }
};

export const calculateFees = (order) => {
  const fees = {};
  fees.maker = {};
  Object.keys(schedule.maker).forEach(tier => {
    const template = schedule.maker[tier];
    fees.maker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });
  fees.taker = {};
  Object.keys(schedule.taker).map(tier => {
    const template = schedule.taker[tier];
    fees.taker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });

  fees.summary = {
    exchange: 'okex',
    low: fees.maker['tier1'].toString(),
    high: fees.maker['tier8'].toString()
  };
  return fees;
};
