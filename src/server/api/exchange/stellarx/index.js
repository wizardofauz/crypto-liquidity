import { Server } from 'stellar-sdk';
import { getTicker, Okex } from '../okex';
const stellar = new Server('https://horizon.stellar.org');

// const book = new Book('XLMETH');
// const eth = new StellarSdk.Asset('ETH', 'GBETHKBL5TCUTQ3JPDIYOZ5RDARTMHMEKIO2QZQ7IOZ4YC5XV3C2IKYU');
// const processStellarBookUpdate = (msg) => {
//   const entry = msg[0];
//   const depth = { asks: {}, bids: {} };
//   for(let ask of entry.asks) {
//     depth.asks[ask.price] = ask.amount;
//   }
//   for(let bid of entry.bids) {
//     depth.bids[bid.price] = bid.amount;
//   }
//   return { symbol: entry.base.asset_code, depth };
// };
// stellar
//   .orderbook(StellarSdk.Asset.native(), eth)
//   .limit(30)
//   .stream({ onmessage: b.addFeed('stellar', processStellarBookUpdate), reconnectTimeout: 10000 });

const subscriptions = {};

export default {

  createSubscription: (tickerPair, book) => {
    const symbol = getTicker(tickerPair);
    if (subscriptions[symbol]) {
      logger.info('Found cached okex subscription for ', symbol);
      subscriptions[symbol].subscriptionCount++;
    } else {
      logger.info('Creating okex subscription for ', symbol);
      const okexApi = new Okex();
      okexApi.addSubscriptionDepth([symbol]);
      okexApi.onMessage(book.addFeed('okex',
        params => {
          return { symbol: symbol, depth: params[0] };
        },
        depth => {
          if (!depth)
            return;
          // console.log('okex', depth);
          const asksPreformat = depth[0].data.asks;
          const bidsPreformat = depth[0].data.bids;
          let unsortedAsks,
            unsortedBids;
          if (asksPreformat) {
            unsortedAsks = {};
            asksPreformat.forEach(tuple => {
              unsortedAsks[tuple[0]] = tuple[1];
            });
          }
          if (bidsPreformat) {
            unsortedBids = {};
            bidsPreformat.forEach(tuple => {
              unsortedBids[tuple[0]] = tuple[1];
            });
          }
          return { bids: unsortedBids, asks: unsortedAsks };
        })
      );
      subscriptions[symbol] = {
        endpoint: okexApi,
        subscriptionCount: 1,
      };
    }
  },

  removeSubscription: (tickerPair) => {
    const symbol = getTicker(tickerPair);
    logger.info('Removing okex subscription for ', symbol);
    subscriptions[symbol].subscriptionCount--;
    if (subscriptions[symbol].subscriptionCount <= 0) {
      logger.info('No more subscribers, destroying okex subscription for ', symbol);
      const { endpoint } = subscriptions[symbol];
      endpoint.terminate();
      delete subscriptions[symbol];
    }
  }

}
