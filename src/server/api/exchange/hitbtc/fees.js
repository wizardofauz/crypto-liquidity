import { Decimal } from 'decimal.js';

const schedule = {
  maker: {
    tier1: { multiplier: 0.0012, text: '0.12%%' },
    tier2: { multiplier: 0.0007, text: '0.07%' },
    tier3: { multiplier: 0.0002, text: '0.02%' },
  },
  taker: {
    tier1: { multiplier: 0.0012, text: '0.12%%' },
    tier2: { multiplier: 0.0007, text: '0.07%' },
    tier3: { multiplier: 0.0002, text: '0.02%' },
  }
};

export const calculateFees = (order) => {
  const fees = {};
  fees.maker = {};
  Object.keys(schedule.maker).forEach(tier => {
    const template = schedule.maker[tier];
    fees.maker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });
  fees.taker = {};
  Object.keys(schedule.taker).map(tier => {
    const template = schedule.taker[tier];
    fees.taker[tier] =
      new Decimal(template.multiplier).times(new Decimal(order.aggregatedPrice)).toString();
  });

  fees.summary = {
    exchange: 'hitbtc',
    low: fees.maker['tier1'].toString(),
    high: fees.maker['tier3'].toString()
  };
  return fees;
};
