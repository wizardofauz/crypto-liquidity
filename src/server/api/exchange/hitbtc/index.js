import request from 'request-promise-native';
import Logger from 'common/utils/logger';
import config from 'common/utils/config';
const subscriptions = {};

const logger = new Logger('hitbtc-wrapper');

export const getTicker = (ticker) => {
  const { left, right } = ticker;
  return `${left.symbol}${right.symbol}`;
};

export default {

  createSubscription: (tickerPair, book) => {
    const symbol = getTicker(tickerPair);

    if (subscriptions[symbol]) {
      logger.info('Found cached hitbtc subscription for ', symbol);
      subscriptions[symbol].subscriptionCount++;
    } else {
      logger.info('Creating hitbtc subscription for ', symbol);

      const handler = book.addFeed('hitbtc', _ => {
        return { depth: _[0] };
      }, depth => {
        let updates = depth;
        const unsortedBids = {}, unsortedAsks = {};
        if (updates.bid) {
          for(let update of updates.bid) {
            const { price, size } = update;
            if (price === 0 || size === 0) {
              continue;
            } else {
              unsortedBids[price] = size;
            }
          }
        }
        if (updates.ask) {
          for(let update of updates.ask) {
            const { price, size } = update;
            if (price === 0 || size === 0) {
              continue;
            } else {
              unsortedAsks[price] = size;
            }
          }
        }
        return { bids: unsortedBids, asks: unsortedAsks };
      });
      const getOrderBook = () => {
        request({
          uri: `${config.exchanges.hitbtc.url}${symbol}`,
          qs: {
            limit: 0
          },
          json: true
        }).then(handler);
      };

      const timer = setInterval(getOrderBook, 5000);
      getOrderBook();
      subscriptions[symbol] = {
        endpoint: timer,
        subscriptionCount: 1,
      };
    }
  },

  removeSubscription: (tickerPair) => {
    const symbol = getTicker(tickerPair);
    logger.info('Removing hitbtc subscription for ', symbol);
    subscriptions[symbol].subscriptionCount--;
    if (subscriptions[symbol].subscriptionCount <= 0) {
      logger.info('No more subscribers, destroying hitbtc subscription for ', symbol);
      const { endpoint } = subscriptions[symbol];
      clearInterval(endpoint);
      delete subscriptions[symbol];
    }
  }

}
