import { EventEmitter } from 'events';
import Logger from 'common/utils/logger';
import Liquidity from '../common/liquidity';
import BookCache from '../cache/bookCache';
import { LIQUIDITY_UPDATE } from '../common/constant';
import ExchangeCache from 'api/cache/exchangeCache';
import { calculateFees } from '../exchange/util';

const logger = new Logger('interest');


export default class Interest extends EventEmitter {

  constructor({ interestId, tickerPair, volume, direction, exchanges }, ctx) {
    super();
    this.ctx = ctx;
    this.tickerPair = tickerPair;
    this.interestId = interestId;
    this.volume = volume;
    this.direction = direction;
    this.exchanges = exchanges;
    this.book = BookCache.getBook(this.tickerPair.left.symbol, this.tickerPair.right.symbol);
    this.init();
  }

  changeVolume(volume) {
    this.volume = volume;
    this.liquidity.removeInterest();
    this.createInterest();
  }

  changeDirection(direction) {
    this.direction = direction;
    this.liquidity.removeInterest();
    this.createInterest();
  }

  createInterest() {
    const updateFees = update => {
      const { trades } = update;
      return calculateFees(trades);
    };
    this.liquidity
      .addInterest(
        update => {
          updateFees(update);
          this.emit(LIQUIDITY_UPDATE, { ...update });
        },
        2000,
        this.volume,
        this.direction,
        this.exchanges);
  }

  init() {
    const exchanges = this.exchanges
      .map(exchangeKey => ExchangeCache.getExchange(exchangeKey));

    const symbol = `${this.tickerPair.left.symbol}/${this.tickerPair.right.symbol}`;
    for (let exchange of exchanges) {
      logger.info(this.ctx,
        `Subscribing to ${exchange.name} for ${symbol}`);
      try {
        exchange.createSubscription(this.tickerPair, this.book);
      } catch(err) {
        logger.error('Error when attempting to subscribe to', exchange.name, symbol, err);
      }
    }
    this.liquidity = new Liquidity(symbol.toUpperCase(), this.book, this.ctx);
    this.createInterest();
  }

  destroy() {
    logger.warn(this.ctx,
      `Destroying interest ${this.interestId} in ${this.tickerPair.left.symbol} ${this.tickerPair.right.symbol}`);
    this.liquidity.removeInterest();
    const exchanges = this.exchanges
      .map(exchangeKey => ExchangeCache.getExchange(exchangeKey));
    for (let exchange of exchanges) {
      exchange.removeSubscription(this.tickerPair);
    }
  }

}
