import request from 'request-promise-native';
import config from 'common/utils/config';

const url = config.feedbackSlackURL;

export const forwardFeedback = (payload) => {

  const options = {
    uri: url,
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    json: true,
    body: {
      text: JSON.stringify(payload, undefined, 4)
    }
  };

  return request(options);
};
