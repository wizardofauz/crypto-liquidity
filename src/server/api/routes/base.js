import Logger from 'common/utils/logger';

const logger = new Logger('base-router');

export default function (router) {
  router
    .get('status', async ctx => {
      try {
        ctx.body = {
          status: 'Up and running',
        };
      } catch (err) {
        logger.error(err.message);
        ctx.status = 500;
        ctx.body = {
          error: 'Error while transcribing data',
        };
      }
    });

}
