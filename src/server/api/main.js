import { Decimal } from 'decimal.js';
import * as Sentry from '@sentry/node';
import app from './server';
import Logger from 'common/utils/logger';
import config from 'common/utils/config';
import attachPrimus from 'api/ws/primus-wrapper';
import RealtimeHandler from 'api/ws/realtime';
import ExchangeCache from 'api/cache/exchangeCache';
import exchanges from 'api/exchange';

if (process.env.NODE_ENV != 'dev') {
  Sentry.init({
    ...config.monitoring.sentry,
    release: __COMMIT_HASH__
  });

  app.on('error', err => {
    Sentry.captureException(err);
  });
}
process.on("uncaughtException", function (err) {
  if (process.env.NODE_ENV != 'dev') {
    Sentry.captureException(err);
  }
  console.error('err uncaught Exception  : ', err);
});

const logger = new Logger('api-main');

logger.info(`Running API with mode NODE_ENV=${process.env.NODE_ENV || 'dev'}`);
const { server, primus } = attachPrimus(app, {
  transformer: 'websockets',
  benchmarking: false,
});

Decimal.set({ precision: 7 });
Decimal.set({ rounding: 0 });

new RealtimeHandler(primus);
Object
  .keys(exchanges)
  .forEach(exchangeKey => {
    const exchange = exchanges[exchangeKey];
    exchange.name = exchangeKey;
    ExchangeCache
      .registerExchange(exchange)
      .then(() => logger.info(`Successfully registered ${exchangeKey}`))
  });

const port = config.web_service.port;
server.listen(port || 3000, () => {
  logger.info(`API Server listening on port ${port || 3000}`);
});
