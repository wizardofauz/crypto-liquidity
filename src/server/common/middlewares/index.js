import cors from 'koa-cors';
import convert from 'koa-convert';
import bodyParser from 'koa-bodyparser';
import conditionalGet from 'koa-conditional-get';
import cacheControl from 'koa-cache-control';
import staticAssets from 'koa-static';
import mount from 'koa-mount';
import etag from 'koa-etag';
import router from 'koa-router';
import timing from 'koa-response-time';
import error from 'koa-json-error';
import cookie from 'koa-cookie';
import logging from 'common/middlewares/logging';
import config from 'common/utils/config';

let prefix = config.web_service.prefix;
export const timingLayer = app =>
  app
    .use(convert(timing()));

export const loggingLayer = (app, logger) =>
  app
    .use(convert(logging(logger.child({ widget_type: 'koa-logger' }))));

export const initialLayer = app =>
  app
    .use(cookie()) // https://github.com/varunpal/koa-cookie
    .use(bodyParser()) // https://github.com/koajs/bodyparser
    .use(convert(conditionalGet())) // https://github.com/koajs/conditional-get
    .use(convert(cacheControl({
      noCache: true,
      noStore: true,
      noTransform: true,
      mustRevalidate: true,
      maxAge: 0
    })))
    .use(convert(etag())); // https://github.com/koajs/etag

export const apiLayer = (app, apiRoutes) => {
  const opts = prefix !== '' ? { prefix } : {};
  const newRouter = router(opts);
  newRouter
    .use(convert(cors())); // https://github.com/koajs/cors
  apiRoutes(newRouter);

  app
    .use(newRouter.routes())
    .use(newRouter.allowedMethods());

  return newRouter;
};

export const errorLayer = app =>
  app
    .use(error());

export const assetsLayer = app => {
  const maxage = 24 * 60 * 60 * 1000;
  const assetsMiddleware = staticAssets(config.ws.assetsDirectory, { gzip: true, maxage });
  app.use(prefix !== '' ? convert(mount(prefix, assetsMiddleware)) : convert(assetsMiddleware));
};
