import Counter from 'passthrough-counter';
import humanize from 'humanize-number';
import bytes from'bytes';
import chalk from 'chalk';

const colorCodes = {
  5: 'red',
  4: 'yellow',
  3: 'cyan',
  2: 'green',
  1: 'green'
};

/**
 * Development logger.
 */

function dev(logger) {
  return function *ret(next) {
    const start = new Date();
    logger.info('  ' + chalk.bold('<--')
      + ' ' + chalk.bold('%s')
      + ' ' + chalk.bold('%s'),
      this.method,
      this.originalUrl);

    try {
      yield next;
    } catch (err) {
      log(this, start, null, err, null, logger);
      throw err;
    }

    const length = this.response.length;
    const body = this.body;
    let counter;
    if (null == length && body && body.readable) {
      this.body = body
        .pipe(counter = Counter())
        .on('error', this.onerror);
    }

    const ctx = this;
    const res = this.res;

    const onfinish = done.bind(null, 'finish');
    const onclose = done.bind(null, 'close');

    res.once('finish', onfinish);
    res.once('close', onclose);

    function done(event){
      res.removeListener('finish', onfinish);
      res.removeListener('close', onclose);
      log(ctx, start, counter ? counter.length : length, null, event, logger);
    }
  }
}

/**
 * Log helper.
 */

function log(ctx, start, len, err, event, logger) {
  // get the status code of the response
  const status = err
    ? (err.status || 500)
    : (ctx.status || 404);

  // set the color of the status code;
  const s = status / 100 | 0;
  const color = colorCodes[s];

  // get the human readable response length
  let length;
  if (~[204, 205, 304].indexOf(status)) {
    length = '';
  } else if (null == len) {
    length = '-';
  } else {
    length = bytes(len);
  }

  const upstream = err ? chalk.red('xxx')
    : event === 'close' ? chalk.yellow('-x-')
    : chalk.white('-->')

  logger.info('  ' + upstream
    + ' ' + chalk.white('%s')
    + ' ' + chalk.bold('%s')
    + ' ' + chalk[color]('%s')
    + ' ' + chalk.white('%s')
    + ' ' + chalk.white('%s'),
    ctx.method,
    ctx.originalUrl,
    status,
    time(start),
    length);
}

/**
 * Show the response time in a human readable format.
 * In milliseconds if less than 10 seconds,
 * in seconds otherwise.
 */

function time(start) {
  let delta = new Date() - start;
  delta = delta < 10000
    ? delta + 'ms'
    : Math.round(delta / 1000) + 's';
  return humanize(delta);
}

export default dev;
