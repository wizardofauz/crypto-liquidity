export default env => {
  return {
    dev: 'dev',
    development: 'dev',
    qa: 'qa',
    test: 'test',
    staging: 'prod',
    uat: 'uat',
    prod: 'prod',
    production: 'prod',
  }[env];
};
