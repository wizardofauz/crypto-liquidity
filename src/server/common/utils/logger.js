import bunyan from 'bunyan';
import format from 'bunyan-format';
import RotatingFileStream from 'bunyan-rotating-file-stream';
import fs from 'fs';
import path from 'path';
import environment from 'common/utils/environment';
import config from 'common/utils/config';

let parentLogger = null;

const getOptions = (env) => {
  const name = config.logs.name;
  if (env === 'dev') {
    return {
      name: name,
      streams: [
        {
          level: 'info',
          stream: format({ outputMode: 'short', levelInString: true })
        }
      ]
    };
  } else if (env !== 'test') {
    const logPath = path.join('.', 'log');
    if (!fs.existsSync(logPath)) {
      fs.mkdirSync(logPath);
    }

    return {
      name: name,
      streams: [
        {
          level: 'info',
          stream: format({ outputMode: 'short', levelInString: true }),
        },
        {
          type: 'raw',
          level: config.web_service.debug ? 'debug' : 'info',
          stream: new RotatingFileStream({
            path: path.join(logPath, `./${name}.log`),
            period: '1d',          // daily rotation
            totalFiles: 10,        // keep 10 back copies
            rotateExisting: true,  // Give ourselves a clean file when we start up, based on period
            threshold: '100m',      // Rotate log files larger than 10 megabytes
            totalSize: '500m',      // Don't keep more than 20mb of archived log files
            gzip: true             // Compress the archive log files to save space
          })
        }
      ]
    };
  } else {
    return {
      name: name,
      streams: [
        {
          level: 'debug',
          path: `./${name}-test.log`  // log DEBUG and above to a file
        }
      ]
    };
  }
};

class Logger {
  constructor(name) {
    if (!parentLogger) {
      const env = environment(process.env.NODE_ENV);
      parentLogger = bunyan.createLogger(getOptions(env));
    }

    const logger = parentLogger.child({ widget_type: name });
    return logger;
  }
}

export default Logger;
