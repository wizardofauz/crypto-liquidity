import config from 'common/utils/config';

export const getBaseURL = ()=> {
  const host = config.api.host;
  const port = config.api.port;
  const prefix = config.api.prefix ? `/${config.api.prefix}` : '';
  return  `http://${host}:${port}${prefix}`;
};

/*
 * memoize.js
 * by @philogb and @addyosmani
 * with further optimizations by @mathias
 * and @DmitryBaranovsk
 * perf tests: http://bit.ly/q3zpG3
 * Released under an MIT license.
 */
export const memoize = (fn) => {
  return function () {
    var args = Array.prototype.slice.call(arguments),
      hash = "",
      i = args.length;
    var currentArg = null;
    while (i--) {
      currentArg = args[i];
      hash += (currentArg === Object(currentArg)) ?
          JSON.stringify(currentArg) : currentArg;
      fn.memoize || (fn.memoize = {});
    }
    return (hash in fn.memoize) ? fn.memoize[hash] :
        fn.memoize[hash] = fn.apply(this, args);
  };
};
