import request from 'request-promise-native';
import config from 'common/utils/config';

export const getAllCoins = async () => {
  const options = {
    uri: config.coins.staticListURL,
    headers: {
      'User-Agent': 'poller.trace.ninja'
    },
    json: true
  };
  return await request(options);
};
