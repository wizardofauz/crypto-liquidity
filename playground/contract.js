import request from 'request-promise-native'
import Stellar from 'stellar-sdk'

const server = new Stellar.Server('https://horizon-testnet.stellar.org');
Stellar.Network.useTestNetwork();

const pairA = Stellar.Keypair.random();
const pairB = Stellar.Keypair.random();

const generateAccount = (keypair) => {
  return request.get({
    uri: 'https://horizon-testnet.stellar.org/friendbot',
    qs: { addr: keypair.publicKey() },
    json: true
  }).then(() => {
    return server.loadAccount(keypair.publicKey())
  }).then(account => {
    console.log('\nBalances for account: ' + keypair.publicKey());
    account.balances.forEach((balance) => {
      console.log('Type:', balance.asset_type, ', Balance:', balance.balance);
    });
    return account;
  });
};

const makePayment = async (accountA, pairA, pairB, amount) => {
  const transaction = new Stellar.TransactionBuilder(accountA)
    .addOperation(Stellar.Operation.payment({
      destination: pairB.publicKey(),
      asset: Stellar.Asset.native(),
      amount: '' + amount
    }))
    .build();
  transaction.sign(pairA);
  console.log("\nXDR format of transaction: ", transaction.toEnvelope().toXDR('base64'));

  try {
    const transactionResult = await server.submitTransaction(transaction);
    console.log('\n\nSuccess! View the transaction at: ');
    console.log(transactionResult._links.transaction.href);
    console.log(JSON.stringify(transactionResult, null, 2));
  } catch (err) {
    console.error('An error has occured:');
    console.error(err);
  }
};

// Create 2 accounts on the test net
Promise.all([generateAccount(pairA), generateAccount(pairB)])
  .then(accounts => {
    const accountA = accounts[0];
    const accountB = accounts[1];
    console.log(accountA);
    console.log(accountB);
    return makePayment(accountA, pairA, pairB, 500);
  });
