const startOrders = 1000;
const startSize = 100000;
const ordersPerTick = 100;
const sizeDiffPerTick = 2000;

function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

const generateTick = (orderCount, size) => {
  const orders = [];
  const orderOfMagnitude = (size / orderCount) * 10;
  // Basic
  for(let i = 0; i < orderCount - 5; i++) {
    // 0..100
    const size = Math.round(Math.random() * orderOfMagnitude);
    const direction = (Math.round(Math.random() * 10) % 2) === 1;
    const order = { size: size, type: direction < 0 ? 'BUY' : 'SELL' };
    orders.push(order);
  }
  // Make up to size
  const sizeSoFar = orders.reduce((current, next) => {
    return { size: next.size + ( current.size * (current.type === 'BUY' ? -1 : 1)) };
  });
  const newChunkSize = Math.round((sizeSoFar.size - size) / 5);
  const newChunkDirection = ((sizeSoFar.size - size) < 0) ? 'SELL' : 'BUY';
  for(let i = 0; i < 5; i++) {
    orders.push({ size: newChunkSize, type: newChunkDirection });
  }
  // Shuffle
  shuffle(orders);
  return orders;
};

const start = generateTick(startOrders, startSize);
console.log(start);

while (true) {
  const updateOrder = generateTick(ordersPerTick, sizeDiffPerTick);
  const overall =
}
