FROM node:8-alpine

RUN mkdir -p /web.svc
WORKDIR /web.svc
ADD . /web.svc

RUN npm install
RUN npm run build:ui
RUN npm run build:server

RUN rm -rf /web.src/src
RUN rm -rf /web.src/stories
RUN rm -rf /web.src/webpack

EXPOSE 3000
CMD ["npm", "run", "docker"]
