import React from 'react';
import { storiesOf } from '@storybook/react';
import { Provider } from 'react-redux';
import { browserHistory } from 'react-router';

import store from 'ui/store/index';
import Help from 'ui/components/common/Help';
import FeesDisplay from 'ui/components/common/FeesDisplay';
import Depth from 'ui/components/common/Depth';

const FEES_TRADES = require('../data/mockTrades');
const DEPTH = require('../data/mockDepth');

export const help = storiesOf('Help', module)
  .addDecorator(story => <Provider store={store}>{story()}</Provider>)
  .add('default', () => (<Help />));

export const fees = storiesOf('FeesDisplay', module)
  .addDecorator(story => <Provider store={store}>{story()}</Provider>)
  .add('default', () => (<FeesDisplay trades={FEES_TRADES.trades}/>));

export const depth = storiesOf('Depth', module)
  .add('default', () => (<Depth depth={DEPTH} exchanges={['huobipro', 'okex', 'binance']}/>));

export default {
  help,
  fees,
  depth,
}
