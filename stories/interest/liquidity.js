import React from 'react';
import { storiesOf } from '@storybook/react';
import { Provider } from 'react-redux';
import { browserHistory } from 'react-router';

import store from 'ui/store/index';
import Liquidity from 'ui/components/interest/Liquidity';

const TICKERS = require('../data/mockTickers');
const INTEREST = require('../data/mockInterest');
const FILLED_LIQUIDITY = require('../data/mockFilledLiquidity');
const interestKey = INTEREST.interest.interestId;

export const liquidity = storiesOf('Liquidity', module)
  .addDecorator(story => {
    store.dispatch({
      type: INTEREST.type,
      payload: INTEREST
    });
    return (<Provider store={store}><div style={{ width: '50%', height: '20rem' }}>{story()}</div></Provider>);
  })
  .add('default', () => {
    store.dispatch({
      type: FILLED_LIQUIDITY.type,
      payload: FILLED_LIQUIDITY
    });
    return (<Liquidity key={interestKey} interestKey={interestKey}/>)
  });

export default liquidity;
