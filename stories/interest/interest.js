import React from 'react';
import { storiesOf } from '@storybook/react';
import { Provider } from 'react-redux';
import { browserHistory } from 'react-router';

import store from 'ui/store/index';
import Interest from 'ui/components/interest/Interest';

const INTEREST = require('../data/mockInterest');
const PENDING_LIQUIDITY = require('../data/mockPendingLiquidity');
const FILLED_LIQUIDITY = require('../data/mockFilledLiquidity');
const interestKey = INTEREST.interest.interestId;

export const interest = storiesOf('Interest', module)
  .addDecorator(story => {
    store.dispatch({
      type: INTEREST.type,
      payload: INTEREST
    });
    return (<Provider store={store}><div style={{ width: '50%' }}>{story()}</div></Provider>);
  })
  .add('loading', () => (<Interest key={interestKey} interestKey={interestKey} />))
  .add('pending', () => {
    store.dispatch({
      type: PENDING_LIQUIDITY.type,
      payload: PENDING_LIQUIDITY
    });
    return (
      <Interest key={interestKey} interestKey={interestKey} />
    );
  })
  .add('filled', () => {
    store.dispatch({
      type: FILLED_LIQUIDITY.type,
      payload: FILLED_LIQUIDITY
    });
    return (
      <Interest key={interestKey} interestKey={interestKey} />
    );
  });

export default interest;
