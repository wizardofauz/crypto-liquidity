import ui from './ui/prod.config.babel';
import server from './server/prod.config.babel';

export default [
  ui,
  server
];
