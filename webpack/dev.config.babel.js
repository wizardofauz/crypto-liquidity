import ui from './ui/dev.config.babel';
import server from './server/dev.config.babel';

export default [
  ui,
  server
];
