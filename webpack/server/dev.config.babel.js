import webpack from 'webpack';
import path from 'path';
import fs from 'fs';

let commitHash = require('child_process')
  .execSync('git describe --long --first-parent')
  .toString();

let nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });

module.exports = {
  context: path.resolve(path.join(__dirname, '../..'), 'src/server'),
  entry: {
    api: './api/main.js',
  },
  optimization: {
    nodeEnv: false
  },
  mode: 'development',
  target: 'node',
  output: {
    path: path.resolve(path.join(__dirname, '../..'), 'dist'),
    filename: '[name].js'
  },
  stats: {
    colors: true,
    reasons: true
  },
  resolve: {
    modules: [
      path.resolve(path.join(__dirname, '../..'), 'src/server'),
      path.resolve(path.join(__dirname, '../..'), 'node_modules')
    ],
    extensions: ['.js', '.json']
  },
  module: {
    rules: [
      // {
      //   enforce: 'pre',
      //   loader: 'eslint-loader',
      //   query: {
      //     configFile: '.eslintrc'
      //   }
      // },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env', 'stage-0']
            }
          }
        ]
      }
    ]
  },
  externals: nodeModules,
  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: true
    }),
    new webpack.DefinePlugin({
      __COMMIT_HASH__: JSON.stringify(commitHash),
    }),
  ],
  devtool: 'eval-cheap-module-source-map'
};
