import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import FaviconsWebpackPlugin from 'favicons-webpack-plugin';
import webpack from "webpack";

let commitHash = require('child_process')
  .execSync('git describe --long --first-parent')
  .toString();

module.exports = {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    historyApiFallback: true,
    hot: true,
    compress: true,
    proxy: {
      '/status': 'http://127.0.0.1:3000',
      '/rt*': {
        target: 'ws://127.0.0.1:3000',
        ws: true,
      },
    }
  },
  context: path.resolve(path.join(__dirname, '../..'), 'src/ui'),
  entry: {
    ui: './index.jsx',
  },
  output: {
    filename: '[name].[hash].bundle.js',
    chunkFilename: '[name].[hash].bundle.js',
    path: path.resolve(__dirname, '../../dist/'),
    publicPath: process.env.NODE_ENV === 'prod' ? './' : '/',
    globalObject: 'this'
  },
  optimization: {
    splitChunks: {
      chunks:'all',
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          chunks: 'initial',
          name: 'vendor',
          priority: 10,
          enforce: true
        }
      }
    }
  },
  resolve: {
    modules: ['./node_modules', './src'],
    extensions: ['.js', '.jsx', '.json', 'css', 'less']
  },
  module: {
    rules: [
      {
        test: /\.(jsx|js)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          plugins: ['transform-runtime', 'transform-class-properties'],
          presets: ['react', 'env', 'stage-0']
        }
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: '[hash]-[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(svg)$/,
        loader: 'file-loader',
        options: {
          name: '[hash].[ext]',
        },
      },
      {
        test: /\.worker\.js$/,
        use: { loader: 'worker-loader' }
      },
      {
        test: /\.(css)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          }
        ]
      },
      {
        test: /\.(css)$/,
        include: /node_modules/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          }
        ]
      }
    ]
  },
  plugins: [
    new FaviconsWebpackPlugin('./images/favicon.png'),
    new webpack.DefinePlugin({
      __COMMIT_HASH__: JSON.stringify(commitHash),
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '../../index.html'),
      filename: 'index.html'
    })
  ]
};
