import path from 'path';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import UglifyJSPlugin from 'uglifyjs-webpack-plugin';
import FaviconsWebpackPlugin from 'favicons-webpack-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';

let commitHash = 'v1.5.0-7-g76016d4';

module.exports = {
  mode: 'production',
  devtool: 'hidden-source-map',
  context: path.resolve(path.join(__dirname, '../..'), 'src/ui'),
  entry: {
    ui: './index.jsx',
  },
  output: {
    filename: '[name].[hash].bundle.js',
    chunkFilename: '[name].[hash].bundle.js',
    path: path.resolve(__dirname, '../../dist/'),
    publicPath: process.env.NODE_ENV === 'prod' ? './' : '/',
    globalObject: 'this'
  },
  optimization: {
    splitChunks: {
      chunks:'all',
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          chunks: 'initial',
          name: 'vendor',
          priority: 10,
          enforce: true
        }
      }
    }
  },
  resolve: {
    modules: ['./node_modules', './src'],
    extensions: ['.js', '.jsx', '.json', 'css', 'less']
  },
  module: {
    rules: [
      {
        test: /\.(jsx|js)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          plugins: ['transform-runtime', 'transform-class-properties'],
          presets: ['react', 'env', 'stage-0']
        }
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: '[hash]-[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(svg)$/,
        loader: 'file-loader',
        options: {
          name: '[hash].[ext]',
        },
      },
      {
        test: /\.worker\.js$/,
        use: { loader: 'worker-loader' }
      },
      {
        test: /\.(css)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          }
        ]
      },
      {
        test: /\.(css)$/,
        include: /node_modules/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['../../dist']),
    new FaviconsWebpackPlugin('./images/favicon.png'),
    new UglifyJSPlugin({
      sourceMap: true
    }),
    new webpack.DefinePlugin({
      __COMMIT_HASH__: JSON.stringify(commitHash),
      __REDUX_DEVTOOLS_EXTENSION__: false,
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '../../index.html'),
      filename: 'index.html'
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      openAnalyzer: false
    })
  ]
};
